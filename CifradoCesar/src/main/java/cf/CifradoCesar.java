/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cf;

import java.util.stream.Collectors;

/**
 *
 * @author CAR
 */
public class CifradoCesar {

    public String cifra(String valor, int clave) {
        String resultado = null;
        if (valor.charAt(0) <= 57) {
            resultado = valor.chars().map(operand -> operand + clave % 10).mapToObj(value -> String.valueOf((char) value))
                    .collect(Collectors.joining());
        } else {
            resultado = valor.chars().map(operand -> operand + clave % 27).mapToObj(value -> String.valueOf((char) value))
                    .collect(Collectors.joining());
        }

        return resultado;
    }

    public String descifra(String valor, int clave) {
        String resultado = null;
        if (valor.charAt(0) <= 57) {
            resultado = valor.chars().map(operand -> operand - clave % 10).mapToObj(value -> String.valueOf((char) value))
                    .collect(Collectors.joining());
        } else {
            resultado = valor.chars().map(operand -> operand - clave % 27).mapToObj(value -> String.valueOf((char) value))
                    .collect(Collectors.joining());
        }

        return resultado;
    }
}

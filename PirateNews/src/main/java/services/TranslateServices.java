package services;


import dao.TranslateDAO;
import modelo.ResponseTranslator;


public class TranslateServices {

    public ResponseTranslator getNoticiaYTraduccir(String year, String month, String day) {
        ResponseTranslator textTranslate = null;
        NewsServices ns = new NewsServices();
        if (null != ns.getNews(year, month, day)) {
            TranslateDAO td = new TranslateDAO();
            textTranslate = td.traducirAPirata(ns.getNews(year, month, day).getLead_paragraph());
        }

        return textTranslate;
    }


}
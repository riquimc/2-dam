package modelo;

public class Headline {
    private String main;
    private String kicker;
    private String content_kicker;
    private String print_headline;
    private String name;
    private String seo;
    private String sub;

    public String getMain() { return main; }
    public void setMain(String value) { this.main = value; }

    public String getKicker() { return kicker; }
    public void setKicker(String value) { this.kicker = value; }

    public String getContent_kicker() { return content_kicker; }
    public void setContent_kicker(String value) { this.content_kicker = value; }

    public String getPrint_headline() { return print_headline; }
    public void setPrint_headline(String value) { this.print_headline = value; }

    public String getName() { return name; }
    public void setName(String value) { this.name = value; }

    public String getSEO() { return seo; }
    public void setSEO(String value) { this.seo = value; }

    public String getSub() { return sub; }
    public void setSub(String value) { this.sub = value; }
}

package modelo;

public class Legacy {
    private String xlarge;
    private int xlargeheight;
    private int xlargewidth;

    public String getXlarge() { return xlarge; }
    public void setXlarge(String value) { this.xlarge = value; }

    public int getXlargeheight() { return xlargeheight; }
    public void setXlargeheight(int value) { this.xlargeheight = value; }

    public int getXlargewidth() { return xlargewidth; }
    public void setXlargewidth(int value) { this.xlargewidth = value; }
}

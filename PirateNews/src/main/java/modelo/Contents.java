package modelo;

public class Contents {
    private String translated;
    private String text;
    private String translation;

    public String getTranslated() { return translated; }
    public void setTranslated(String value) { this.translated = value; }

    public String getText() { return text; }
    public void setText(String value) { this.text = value; }

    public String getTranslation() { return translation; }
    public void setTranslation(String value) { this.translation = value; }
}

package modelo;

import java.util.List;

public class Doc {
    private List<Article> docs;

    public List<Article> getDocs() {
        return docs;
    }

    public void setDocs(List<Article> docs) {
        this.docs = docs;
    }
}

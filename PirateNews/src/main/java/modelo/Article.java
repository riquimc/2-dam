package modelo;

import java.util.List;

public class Article {
    private String section_name;
    private String lead_paragraph;
    private List<Object> blog;
    private String web_url;
    private String snippet;
    private int print_page;
    private String source;
    private List<Multimedia> multimedia;
    private Headline headline;
    private List<Keyword> keywords;
    private String pub_date;
    private String document_type;
    private String news_desk;
    private Byline byline;
    private String type_of_material;
    private String _id;
    private int word_count;
    private int score;
    private String uri;


    public String getSection_name() {
        return section_name;
    }

    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    public String getLead_paragraph() {
        return lead_paragraph;
    }

    public void setLead_paragraph(String lead_paragraph) {
        this.lead_paragraph = lead_paragraph;
    }

    public List<Object> getBlog() {
        return blog;
    }

    public void setBlog(List<Object> blog) {
        this.blog = blog;
    }

    public String getWeb_url() { return web_url; }
    public void setWeb_url(String value) { this.web_url = value; }

    public String getSnippet() { return snippet; }
    public void setSnippet(String value) { this.snippet = value; }

    public int getPrint_page() { return print_page; }
    public void setPrint_page(int value) { this.print_page = value; }

    public String getSource() { return source; }
    public void setSource(String value) { this.source = value; }

    public List<Multimedia> getMultimedia() { return multimedia; }
    public void setMultimedia(List<Multimedia> value) { this.multimedia = value; }

    public Headline getHeadline() { return headline; }
    public void setHeadline(Headline value) { this.headline = value; }

    public List<Keyword> getKeywords() { return keywords; }
    public void setKeywords(List<Keyword> value) { this.keywords = value; }

    public String getPub_date() { return pub_date; }
    public void setPub_date(String value) { this.pub_date = value; }

    public String getDocument_type() { return document_type; }
    public void setDocument_type(String value) { this.document_type = value; }

    public String getNews_desk() { return news_desk; }
    public void setNews_desk(String value) { this.news_desk = value; }

    public Byline getByline() { return byline; }
    public void setByline(Byline value) { this.byline = value; }

    public String getType_of_material() { return type_of_material; }
    public void setType_of_material(String value) { this.type_of_material = value; }

    public String get_id() { return _id; }
    public void set_id(String value) { this._id = value; }

    public int getWord_count() { return word_count; }
    public void setWord_count(int value) { this.word_count = value; }

    public int getScore() { return score; }
    public void setScore(int value) { this.score = value; }

    public String getUri() { return uri; }
    public void setUri(String value) { this.uri = value; }
}

package modelo;

public class Multimedia {
    private int rank;
    private String subType;
    private String caption;
    private String credit;
    private String type;
    private String url;
    private int height;
    private int width;
    private Legacy legacy;
    private String crop_name;

    public int getRank() { return rank; }
    public void setRank(int value) { this.rank = value; }

    public String getSubType() { return subType; }
    public void setSubType(String value) { this.subType = value; }

    public String getCaption() { return caption; }
    public void setCaption(String value) { this.caption = value; }

    public String getCredit() { return credit; }
    public void setCredit(String value) { this.credit = value; }

    public String getType() { return type; }
    public void setType(String value) { this.type = value; }

    public String getURL() { return url; }
    public void setURL(String value) { this.url = value; }

    public int getHeight() { return height; }
    public void setHeight(int value) { this.height = value; }

    public int getWidth() { return width; }
    public void setWidth(int value) { this.width = value; }

    public Legacy getLegacy() { return legacy; }
    public void setLegacy(Legacy value) { this.legacy = value; }

    public String getCrop_name() { return crop_name; }
    public void setCrop_name(String value) { this.crop_name = value; }
}

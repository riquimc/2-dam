package modelo;

import java.util.List;

public class Byline {
    private String original;
    private String organization;
    private List<Person> person;

    public String getOriginal() { return original; }
    public void setOriginal(String value) { this.original = value; }

    public String getOrganization() { return organization; }
    public void setOrganization(String value) { this.organization = value; }

    public List<Person> getPerson() { return person; }
    public void setPerson(List<Person> value) { this.person = value; }
}

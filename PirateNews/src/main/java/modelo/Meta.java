package modelo;

public class Meta {
    private long hits;
    private long offset;
    private long time;

    public long getHits() {
        return hits;
    }

    public void setHits(long value) {
        this.hits = value;
    }

    public long getOffset() {
        return offset;
    }

    public void setOffset(long value) {
        this.offset = value;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long value) {
        this.time = value;
    }
}

package modelo;

public class Keyword {
    private String name;
    private String value;
    private int rank;
    private String major;

    public String getName() { return name; }
    public void setName(String value) { this.name = value; }

    public String getValue() { return value; }
    public void setValue(String value) { this.value = value; }

    public int getRank() { return rank; }
    public void setRank(int value) { this.rank = value; }

    public String getMajor() { return major; }
    public void setMajor(String value) { this.major = value; }
}

package modelo;

import java.util.List;

public class Response {
    private List<Article> docs;
    private Meta meta;

    public List<Article> getDocs() {
        return docs;
    }

    public void setDocs(List<Article> value) {
        this.docs = value;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta value) {
        this.meta = value;
    }
}

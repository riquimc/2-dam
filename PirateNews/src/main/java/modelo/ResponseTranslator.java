package modelo;

public class ResponseTranslator {
    private Succes succes;
    private Contents contents;

    public Succes getSucces() {
        return succes;
    }

    public void setSucces(Succes succes) {
        this.succes = succes;
    }

    public Contents getContents() {
        return contents;
    }

    public void setContents(Contents contents) {
        this.contents = contents;
    }
}

package modelo;

public class Person {
    private String firstname;
    private String middlename;
    private String lastname;
    private String qualifier;
    private String title;
    private String role;
    private String organization;
    private int rank;

    public String getFirstname() { return firstname; }
    public void setFirstname(String value) { this.firstname = value; }

    public String getMiddlename() { return middlename; }
    public void setMiddlename(String value) { this.middlename = value; }

    public String getLastname() { return lastname; }
    public void setLastname(String value) { this.lastname = value; }

    public String getQualifier() { return qualifier; }
    public void setQualifier(String value) { this.qualifier = value; }

    public String getTitle() { return title; }
    public void setTitle(String value) { this.title = value; }

    public String getRole() { return role; }
    public void setRole(String value) { this.role = value; }

    public String getOrganization() { return organization; }
    public void setOrganization(String value) { this.organization = value; }

    public int getRank() { return rank; }
    public void setRank(int value) { this.rank = value; }
}

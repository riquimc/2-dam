package dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import modelo.ResponseTranslator;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import retrofit.TraducirTexto;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import services.TranslateServices;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TranslateDAO {
    public ResponseTranslator traducirAPirata(String text) {


        ResponseTranslator textTranslate = null;
        try {
            final String BASE_URL = "https://api.funtranslations.com/translate/";

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

            OkHttpClient clientOK = new OkHttpClient.Builder()
                    .cookieJar(new JavaNetCookieJar(cookieManager))
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(clientOK)
                    .build();

            TraducirTexto traducirTexto = retrofit.create(TraducirTexto.class);

            Call<ResponseTranslator> call = traducirTexto.traducirAPirata(text);

            textTranslate = call.execute().body();
        } catch (Exception ex) {
            Logger.getLogger(TranslateServices.class.getName()).log(Level.SEVERE, null, ex);
        }
        return textTranslate;
    }

}

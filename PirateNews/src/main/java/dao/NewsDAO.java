package dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import modelo.Article;
import modelo.ResponseNewYorkTimes;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit.NewsTimes;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import services.TranslateServices;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NewsDAO {
    public Article getNews(String year, String month, String day) {
        Article news = null;
        try {
            final String BASE_URL = "https://api.nytimes.com/svc/archive/v1/";

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            CookieManager cookieManager = new CookieManager();
            cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

            OkHttpClient clientOK = new OkHttpClient.Builder()
                    .cookieJar(new JavaNetCookieJar(cookieManager))
                    .addInterceptor(chain -> {
                                Request original = chain.request();

                                Request.Builder builder1 = original.newBuilder()
                                        .url(original.url().newBuilder().addQueryParameter("api-key", "G9HvUGs8d9ySHvrBbo9GwlvyEZDbtQBf").build());
                                Request request = builder1.build();
                                return chain.proceed(request);
                            }
                    )
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(clientOK)
                    .build();

            NewsTimes noticia = retrofit.create(NewsTimes.class);

            Call<ResponseNewYorkTimes> call = noticia.imprimirNoticia(year, month);

            StringBuilder dayS = new StringBuilder();
            StringBuilder monthS = new StringBuilder();
            if (Integer.parseInt(month) < 10 && Integer.parseInt(day) < 10) {
                monthS.append("0" + month);
                dayS.append("0" + day);
            } else if (Integer.parseInt(month) < 10) {
                monthS.append("0" + month);
            } else if (Integer.parseInt(day) < 10) {
                dayS.append("0" + day);
            }

            news = call.execute().body().getResponse().getDocs()
                    .stream().filter(article -> article.getPub_date().contains(year + "-" + monthS + "-" + dayS))
                    .findFirst().orElse(null);


        } catch (Exception ex) {
            Logger.getLogger(TranslateServices.class.getName()).log(Level.SEVERE, null, ex);
        }

        return news;
    }

}

package retrofit;




import modelo.ResponseTranslator;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TraducirTexto {
    @POST("pirate")
    Call<ResponseTranslator> traducirAPirata(@Query("text") String text);
}

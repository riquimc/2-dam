package retrofit;


import modelo.ResponseNewYorkTimes;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface NewsTimes {
    @GET("{year}/{month}.json")
    Call<ResponseNewYorkTimes> imprimirNoticia(@Path("year") String year, @Path("month") String month);
}

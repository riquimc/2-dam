package fx.controller;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import services.TranslateServices;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PantallaInicioController implements Initializable {
    private Alert alert;

    @FXML
    public TextArea fxTextTraducido;
    @FXML
    public TextField fxMes;
    @FXML
    public TextField fxYear;
    @FXML
    public TextField fxDia;

    private boolean checkEmpty() {
        return !fxMes.getText().isEmpty() || !fxYear.getText().isEmpty() || !fxDia.getText().isEmpty();
    }

    private boolean cheackNumber() {
        boolean number = true;
        try {
            Integer.parseInt(fxMes.getText());
            Integer.parseInt(fxDia.getText());
            Integer.parseInt(fxMes.getText());
        } catch (Exception ex) {
            number = false;
        }
        return number;
    }

    @FXML
    public void traducir() {
        if (checkEmpty()) {
            if (cheackNumber()) {
                TranslateServices transT = new TranslateServices();
                Task<String> task = new Task<>() {
                    @Override
                    protected String call() throws Exception {
                        String resultado;
                        try {
                            resultado = transT.getNoticiaYTraduccir(fxYear.getText(), fxMes.getText(), fxDia.getText()).getContents()
                                    .getTranslated().replace(".", ".\n");
                        } catch (Exception e) {
                            resultado = null;
                        }
                        return resultado;
                    }
                };

                task.setOnSucceeded(workerStateEvent -> {
                    if (null != task.getValue()) {
                        fxTextTraducido.clear();
                        fxTextTraducido.setText(task.getValue());
                    } else {
                        alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Error");
                        alert.setContentText("El articulo de la fecha indicada o no existe o no contiene titular");
                        alert.showAndWait();
                    }
                });

                task.setOnFailed(workerStateEvent -> {
                    Logger.getLogger("PantallaInicio")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException());
                });
                ExecutorService executorService = Executors.newFixedThreadPool(1);
                executorService.submit(task);
            } else {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Intraduzca números");
                alert.setHeaderText("Intraduzca números");
                alert.setContentText("Porfavor introduzca solo números");
                alert.showAndWait();
            }
        } else {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Campos vacios");
            alert.setHeaderText("Campos vacios");
            alert.setContentText("Rellene todos los campos");
            alert.showAndWait();
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fxTextTraducido.setEditable(false);
    }


}

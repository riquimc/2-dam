package fx.controllers;

import cf.CifradoCesar;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.skin.TableCellSkin;
import okhttp3.*;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InformacionUsuarioController implements Initializable {

    @FXML
    public Label fxInfoUser;

    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }

    public void fillInfo(OkHttpClient clientOK, ExecutorService executor) {
        Task<String> tarea = new Task<String>() {
            @Override
            protected String call() throws Exception {

                HttpUrl.Builder urlBuilder
                        = HttpUrl.parse("http://localhost:8080/WebSecreta_war/" + "privado/encriptado/" + "informacionUsuario").newBuilder();

                String url = urlBuilder.build().toString();

                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Call call = clientOK.newCall(request);
                try (Response response = call.execute()) {
                    return response.body().string();
                }
            }
        };
        tarea.setOnSucceeded(workerStateEvent -> {
            CifradoCesar cf = new CifradoCesar();
            fxInfoUser.setText(cf.descifra(tarea.getValue(), Integer.parseInt(inicio.getFxCLave().getText())));
        });

        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("PantallaInicio")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
        });
        executor.submit(tarea);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}

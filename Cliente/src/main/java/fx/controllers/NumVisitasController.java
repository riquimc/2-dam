package fx.controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import okhttp3.*;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NumVisitasController implements Initializable {
    @FXML
    public TextArea fxVisitas;

    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }


    public void cargarVisitas(OkHttpClient clientOK, ExecutorService executorService) {

        Task<String> tarea = new Task<String>() {
            @Override
            protected String call() throws Exception {

                HttpUrl.Builder urlBuilder
                        = HttpUrl.parse("http://localhost:8081/ejerciciosSesion_war/" + "sesion").newBuilder();

                String url = urlBuilder.build().toString();

                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Call call = clientOK.newCall(request);

                try(Response response = call.execute()) {
                    return response.body().string();
                }
            }
        };

        tarea.setOnSucceeded(workerStateEvent -> {
            fxVisitas.setText(tarea.getValue());

        });
        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("PantallaInicio")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
        });
        executorService.submit(tarea);
}


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}

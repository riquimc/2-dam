package fx.controllers;

import cf.CifradoCesar;
import filtro.LogInFilter;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import okhttp3.*;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PantallaInicioController implements Initializable {

    private OkHttpClient clientOK;
    private ExecutorService executorService;

    public OkHttpClient getClientOK() {
        return clientOK;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

    @FXML
    public AnchorPane pantallaLogIn;

    @FXML
    private TextField fxCLave;

    public TextField getFxCLave() {
        return fxCLave;
    }

    @FXML
    public MenuBar fxBarra;
    @FXML
    private BorderPane fxRoot;
    @FXML
    private AnchorPane visitas;
    @FXML
    private NumVisitasController visitasController;
    @FXML
    private AnchorPane InfoUser;
    @FXML
    private InformacionUsuarioController InfoUserController;
    @FXML
    private AnchorPane pisosFrancosr;
    @FXML
    private PisosFrancosController pisosFrancosController;

    @FXML
    private AnchorPane informes;
    @FXML
    private InformesController informesController;


    @FXML
    private TextField fxUserName;
    @FXML
    private PasswordField fxPassword;

    @FXML
    public void logIn() {
        Task<String> tarea = new Task<>() {

            @Override
            protected String call() throws Exception {
                try {
                    HttpUrl.Builder urlBuilder
                            = HttpUrl.parse("http://localhost:8080/WebSecreta_war/" + "logIn").newBuilder();
                    CifradoCesar cf = new CifradoCesar();
                    int clave = Integer.parseInt(fxCLave.getText());
                    urlBuilder.addQueryParameter("user", cf.cifra(fxUserName.getText(), clave));
                    urlBuilder.addQueryParameter("pass", cf.cifra(fxPassword.getText(), clave));
                    urlBuilder.addQueryParameter("clave", fxCLave.getText());

                    String url = urlBuilder.build().toString();
                    Request request = new Request.Builder()
                            .url(url)
                            .build();

                    Call call = clientOK.newCall(request);
                    try (Response response = call.execute()) {
                        return response.body().string();
                    }
                } catch (Exception e) {
                    Logger.getLogger(LogInFilter.class.getName()).log(Level.SEVERE, null, e);
                    return "ERROR";
                }
            }
        };

        tarea.setOnSucceeded(workerStateEvent -> {
            if (tarea.getValue().equals("YES")) {
                fxBarra.setVisible(true);
                fxLoadVisitasScene();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("ERROR");
                alert.setTitle("Error");
                alert.setContentText("Ha ocurrido un error porfavor revise los campos");
                alert.showAndWait();
            }
        });
        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("PantallaInicio")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
        });
        executorService.submit(tarea);
    }

    @FXML
    private void fxLoadVisitasScene() {
        fxRoot.setCenter(visitas);
        visitasController.cargarVisitas(clientOK, executorService);
    }

    @FXML
    public void loadInforUserScene() {
        fxRoot.setCenter(InfoUser);
        InfoUserController.fillInfo(clientOK, executorService);
    }

    private void loadVisitas() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/numVisitas.fxml"));
            visitas = loaderMenu.load();
            visitasController
                    = loaderMenu.getController();

            visitasController.setInicio(this);

        } catch (IOException ex) {
            Logger.getLogger(NumVisitasController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadInformes() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/informes.fxml"));
            informes = loaderMenu.load();
            informesController
                    = loaderMenu.getController();

            informesController.setInicio(this);

        } catch (IOException ex) {
            Logger.getLogger(InformesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadPisos() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/pisosFrancos.fxml"));
            pisosFrancosr = loaderMenu.load();
            pisosFrancosController
                    = loaderMenu.getController();

            pisosFrancosController.setInicio(this);

        } catch (IOException ex) {
            Logger.getLogger(PisosFrancosController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadInfoUser() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/informacionUsuario.fxml"));
            InfoUser = loaderMenu.load();
            InfoUserController
                    = loaderMenu.getController();

            InfoUserController.setInicio(this);

        } catch (IOException ex) {
            Logger.getLogger(InformacionUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadPantallaLogIn() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/pantallaInicio.fxml"));
            pantallaLogIn = loaderMenu.load();
        } catch (IOException ex) {
            Logger.getLogger(InformacionUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void loadInformesScene() {
        fxRoot.setCenter(informes);
    }

    @FXML
    public void loadPisosScene() {
        fxRoot.setCenter(pisosFrancosr);
        pisosFrancosController.loadPisos(clientOK, executorService);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fxBarra.setVisible(false);
        loadVisitas();
        loadInfoUser();
        loadInformes();
        loadPisos();

        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        clientOK = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .build();

        executorService = Executors.newFixedThreadPool(1);
    }

    @FXML
    public void CerrarSesion() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation Dialog");
        alert.setHeaderText("Cerrar sesión");
        alert.setContentText("¿Quieres cerrar sesión?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            Task<String> tarea = new Task<>() {
                @Override
                protected String call() throws Exception {

                    HttpUrl.Builder urlBuilder
                            = HttpUrl.parse("http://localhost:8080/WebSecreta_war/" + "privado/" + "cerrarSesion").newBuilder();

                    String url = urlBuilder.build().toString();

                    Request request = new Request.Builder()
                            .url(url)
                            .build();

                    Call call = clientOK.newCall(request);
                    try (Response response = call.execute()) {
                        return response.body().string();
                    }
                }
            };
            tarea.setOnSucceeded(workerStateEvent -> {
                fxPassword.clear();
                fxUserName.clear();
                fxCLave.clear();
                fxRoot.setCenter(pantallaLogIn);
            });

            tarea.setOnFailed(workerStateEvent -> {
                Logger.getLogger("PantallaInicio")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException());
            });
            executorService.submit(tarea);

        }
    }
}

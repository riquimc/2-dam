package fx.controllers;

import cf.CifradoCesar;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import okhttp3.*;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InformesController implements Initializable {

    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }

    @FXML
    public ComboBox fxInformes;
    @FXML
    public TextArea fxInformesText;

    @FXML
    public void fxMostrarInformes() {
        Task<String> tarea = new Task<String>() {
            @Override
            protected String call() throws Exception {

                HttpUrl.Builder urlBuilder
                        = HttpUrl.parse("http://localhost:8080/WebSecreta_war/" + "privado/encriptado/" + "elegirInformes").newBuilder();
                CifradoCesar cf = new CifradoCesar();
                urlBuilder.addQueryParameter("numInforme", cf.cifra(fxInformes.getSelectionModel().getSelectedItem().toString(),
                        Integer.parseInt(inicio.getFxCLave().getText())));
                String url = urlBuilder.build().toString();

                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Call call = inicio.getClientOK().newCall(request);
                try (Response response = call.execute()) {
                    return response.body().string();
                }
            }
        };
        tarea.setOnSucceeded(workerStateEvent -> {
            CifradoCesar cf = new CifradoCesar();
            fxInformesText.setText(cf.descifra(tarea.getValue(), Integer.parseInt(inicio.getFxCLave().getText())));
        });

        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("InformesController")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
        });
        inicio.getExecutorService().submit(tarea);
    }

    private void loadCombo() {
        fxInformes.getItems().add(1);
        fxInformes.getItems().add(2);
        fxInformes.getItems().add(3);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadCombo();
    }


}

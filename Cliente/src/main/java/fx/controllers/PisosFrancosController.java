package fx.controllers;

import cf.CifradoCesar;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import okhttp3.*;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PisosFrancosController implements Initializable {

    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }

    @FXML
    public Label fxPisos;

    public void loadPisos(OkHttpClient clientOK, ExecutorService executorService){
        Task<String> tarea = new Task<String>() {
            @Override
            protected String call() throws Exception {

                HttpUrl.Builder urlBuilder
                        = HttpUrl.parse("http://localhost:8080/WebSecreta_war/" + "privado/encriptado/" + "mostrarPisos").newBuilder();

                String url = urlBuilder.build().toString();

                Request request = new Request.Builder()
                        .url(url)
                        .build();

                Call call = clientOK.newCall(request);
                try (Response response = call.execute()) {
                    return response.body().string();
                }
            }
        };
        tarea.setOnSucceeded(workerStateEvent -> {
            CifradoCesar cf = new CifradoCesar();
            fxPisos.setText(cf.descifra(tarea.getValue(), Integer.parseInt(inicio.getFxCLave().getText())));
        });

        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("PantallaInicio")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
        });
        executorService.submit(tarea);

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}

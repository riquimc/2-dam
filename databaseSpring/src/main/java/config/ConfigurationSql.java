/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 *
 * @author dam2
 */
public class ConfigurationSql {

    private static ConfigurationSql instance = null;
    private Properties p;

    private ConfigurationSql() {
        Path p1 = Paths.get("config/my-properties.xml");
        p = new Properties();
        InputStream propertiesStream;
        try {
            propertiesStream = Files.newInputStream(p1);
            p.loadFromXML(propertiesStream);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static ConfigurationSql getInstance() {

        if (instance == null) {
            instance = new ConfigurationSql();
        }
        return instance;
    }

    public String getProperty(String clave) {
        return p.getProperty(clave);
    }
}

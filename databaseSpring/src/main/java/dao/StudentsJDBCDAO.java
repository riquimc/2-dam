/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import model.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 * @author oscar
 */
public class StudentsJDBCDAO implements DAOStudent{

    private ResultSet rs;
    private ResultSet rs1;
    private PreparedStatement pstmt;
    private DBConnection db;
    private Connection con;

    private static final String SELECT_STUDENTS_QUERY = "select * from student";
    private static final String SELECT_MEMBER_STUDENTS_QUERY = "select * from `member` inner join student on student.IdStudent = `member`.IdMember";

//    public List<Student> getAllStudents() {
//        db = new DBConnection();
//        List<Student> students = new ArrayList<>();
//
//        try {
//            this.con = db.getConnection();
//            pstmt = con.prepareStatement(SELECT_STUDENTS_QUERY);
//            rs = pstmt.executeQuery();
//            pstmt = con.prepareStatement(SELECT_MEMBER_STUDENTS_QUERY);
//            rs1 = pstmt.executeQuery();
//
//            while (rs.next()) {
//                rs1.next();
//                int idStudent = rs.getInt(1);
//                String schoolYear = rs.getString(2);
//                String nif = rs1.getString(2);
//                String name = rs1.getString(3);
//                String address = rs1.getString(4);
//                String phone = rs1.getString(5);
//                Date dateOfBirth = rs1.getDate(6);
//                String idUser = rs1.getString(7);
//
//                students.add(new Student(schoolYear, idStudent, nif, name,
//                        address, phone, LocalDate.parse(dateOfBirth.toString()), idUser));
//            }
//
//        } catch (Exception ex) {
//            Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.releaseResource(rs1);
//            db.closeConnection(con);
//        }
//        return students;
//    }
//
//    public boolean checkStudentHasSubjects(Student student) {
//        db = new DBConnection();
//        boolean succes = false;
//
//        try {
//            this.con = db.getConnection();
//
//            pstmt = con.prepareStatement("select * from `subject`"
//                    + " inner join enroll on enroll.idSubject = subject.idsubject "
//                    + "inner join student on student.idstudent = enroll.idStudent "
//                    + "where student.idstudent = ?");
//            pstmt.setInt(1, student.getIdMember());
//            rs = pstmt.executeQuery();
//            if (rs.next() == false) {
//                succes = false;
//            } else {
//                succes = true;
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//
//        return succes;
//    }
//
//    public boolean addStudent(Student student, String password) {
//        db = new DBConnection();
//        boolean succes = false;
//
//        try {
//            this.con = db.getConnection();
//            con.setAutoCommit(false);
//
//            pstmt = con.prepareStatement("insert into user "
//                    + "(iduser,passwd,idTypeUser) "
//                    + "values (?,?,?)");
//
//            pstmt.setString(1, student.getIdUser());
//            pstmt.setString(2, password);
//            pstmt.setInt(3, 2);
//            pstmt.executeUpdate();
//
//            pstmt = con.prepareStatement("insert into `member` "
//                    + "(NIF,fullName,Address,phone,DateOfBirth,IdUser) "
//                    + "values (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
//
//            pstmt.setString(1, student.getNIF());
//            pstmt.setString(2, student.getFullName());
//            pstmt.setString(3, student.getAddress());
//            pstmt.setString(4, student.getPhone());
//            pstmt.setDate(5, java.sql.Date.valueOf(student.getDateOfBirth()));
//            pstmt.setString(6, student.getIdUser());
//
//            pstmt.executeUpdate();
//            rs = pstmt.getGeneratedKeys();
//
//            String key = null;
//
//            if (rs != null && rs.next()) {
//                key = rs.getString(1);
//            }
//
//            pstmt = con.prepareStatement("insert into student "
//                    + "(idstudent,schoolYear) "
//                    + "values (?,?)");
//
//            pstmt.setString(1, key);
//            pstmt.setString(2, student.getSchoolYear());
//            pstmt.executeUpdate();
//
//            con.commit();
//            succes = true;
//        } catch (Exception ex) {
//            Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                con.setAutoCommit(true);
//            } catch (SQLException ex) {
//                Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//
//        return succes;
//    }
//
//    public boolean deleteStudent(Student student) {
//        db = new DBConnection();
//        boolean succes = false;
//        try {
//
//            this.con = db.getConnection();
//            con.setAutoCommit(false);
//            pstmt = con.prepareStatement("delete from `enroll` where enroll.idStudent = ?");
//
//            pstmt.setInt(1,
//                    student.getIdMember());
//            pstmt.executeUpdate();
//
//            pstmt = con.prepareStatement("delete from `student` where idstudent = ?");
//
//            pstmt.setInt(1,
//                    student.getIdMember());
//            pstmt.executeUpdate();
//
//            pstmt = con.prepareStatement("delete from `member` where idMember = ?");
//
//            pstmt.setInt(1,
//                    student.getIdMember());
//            pstmt.executeUpdate();
//
//            pstmt = con.prepareStatement("delete from user where iduser = ?");
//
//            pstmt.setString(1,
//                    student.getIdUser());
//            pstmt.executeUpdate();
//
//            con.commit();
//            succes = true;
//        } catch (SQLException ex) {
//            Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                con.setAutoCommit(true);
//            } catch (SQLException ex) {
//                Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//        return succes;
//
//    }
//
//    public boolean updateStudent(Student student, String password) {
//        db = new DBConnection();
//        boolean succes = false;
//        try {
//
//            this.con = db.getConnection();
//            con.setAutoCommit(false);
//
//            pstmt = con.prepareStatement("update student "
//                    + "set schoolYear = ? where idstudent = ?");
//
//            pstmt.setString(1,
//                    student.getSchoolYear());
//            pstmt.setInt(2, student.getIdMember());
//            pstmt.executeUpdate();
//
//            pstmt = con.prepareStatement("update `member` "
//                    + "set nif = ?, fullName = ?, address = ?, phone = ?, dateOfBirth = ?,idUser = ?"
//                    + " where idMember = ?");
//
//            pstmt.setString(1,
//                    student.getNIF());
//            pstmt.setString(2,
//                    student.getFullName());
//            pstmt.setString(3,
//                    student.getAddress());
//            pstmt.setString(4,
//                    student.getPhone());
//            pstmt.setDate(5,
//                    java.sql.Date.valueOf(student.getDateOfBirth()));
//            pstmt.setString(6,
//                    student.getIdUser());
//
//            pstmt.setInt(7, student.getIdMember());
//            pstmt.executeUpdate();
//
//            pstmt = con.prepareStatement("update user "
//                    + "set iduser = ?, passwd = ? where iduser = ?");
//            pstmt.setString(1, student.getIdUser());
//            pstmt.setString(2, password);
//            pstmt.setString(3, student.getIdUser());
//            pstmt.executeUpdate();
//
//            con.commit();
//            succes = true;
//        } catch (SQLException ex) {
//            Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                con.setAutoCommit(true);
//            } catch (SQLException ex) {
//                Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//
//        return succes;
//    }
//
//    public List<Integer> getMarks(int idSubject, int idStudent) {
//        db = new DBConnection();
//        List<Integer> marks = new ArrayList<>();
//
//        try {
//            this.con = db.getConnection();
//            pstmt = con.prepareStatement("select Mark from enroll "
//                    + "where idStudent = ? and idSubject = ?");
//
//            pstmt.setInt(1, idStudent);
//            pstmt.setInt(2, idSubject);
//            rs = pstmt.executeQuery();
//
//            while (rs.next()) {
//                marks.add(rs.getInt(1));
//            }
//
//        } catch (Exception ex) {
//            Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.releaseResource(rs1);
//            db.closeConnection(con);
//        }
//        return marks;
//    }
//
//    public Student getStudent(String iduser) {
//        db = new DBConnection();
//        Student s = null;
//        try {
//            this.con = db.getConnection();
//            pstmt = con.prepareStatement("select * from student "
//                    + "inner join `meber` on `member`.idMember = student.idstudent "
//                    + "inner join user on user.iduser = `member`.idUser "
//                    + "where user.iduser = ?");
//            pstmt.setString(1, iduser);
//            rs = pstmt.executeQuery();
//
//            pstmt = con.prepareStatement("select * from `meber` "
//                    + "inner join student on student.idstudent = `member`.idMember "
//                    + "inner join user on user.iduser = `member`.idUser "
//                    + "where user.iduser = ?");
//            pstmt.setString(1, iduser);
//            rs1 = pstmt.executeQuery();
//
//            while (rs.next()) {
//                rs1.next();
//                int idStudent = rs.getInt(1);
//                String schoolYear = rs.getString(2);
//                String nif = rs1.getString(2);
//                String name = rs1.getString(3);
//                String address = rs1.getString(4);
//                String phone = rs1.getString(5);
//                Date dateOfBirth = rs1.getDate(6);
//                String idUser = rs1.getString(7);
//
//                s = new Student(schoolYear, idStudent, nif, name,
//                        address, phone, LocalDate.parse(dateOfBirth.toString()), idUser);
//            }
//
//        } catch (Exception ex) {
//            Logger.getLogger(UserJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//        return s;
//    }

    

    @Override
    public List<Student> getAll() {
 List<Student> l = new ArrayList<>();
        return l;
    }

    @Override
    public void save(Student t) {
    }

    @Override
    public boolean update(Student t,Student oldStudent) {
        return true;
    }

    @Override
    public boolean delete(Student t) {
        return true;
    }

    @Override
    public List<Integer> getMarks(int idStudent, int idSubject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Student get(User u) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

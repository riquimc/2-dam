/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Student;
import model.User;

/**
 *
 * @author dam2
 */
public interface DAOStudent {

    Student get(User u);

    List<Student> getAll();

    void save(Student t);

    boolean update(Student t,Student oldStudent);

    boolean delete(Student t);
    
    List<Integer> getMarks(int idStudent, int idSubject);

}

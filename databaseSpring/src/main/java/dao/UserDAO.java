/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.User;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author dam2
 */
public class UserDAO implements DAOUser {

    private static final String SELECT_USERS = "select * from user";

    @Override
    public List<User> getAll() {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query(SELECT_USERS, BeanPropertyRowMapper.newInstance(User.class));
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Student;
import model.Subject;

/**
 *
 * @author CAR
 */
public class SubjectJDBCDAO implements DAOSubject {

    private ResultSet rs;
    private ResultSet rs1;
    private PreparedStatement pstmt;
    private DBConnection db;
    private Connection con;

    private static final String SELECT_SUBJECTS_QUERY = "select * from subject";

    private static final String SELECT_STUDENTS_QUERY = "select * from student inner join enroll on enroll.idStudent = student.idstudent "
            + "inner join subject on subject.idsubject = enroll.idSubject";

    private static final String SELECT_MEMBER_STUDENTS_QUERY = "select * from `member` inner join enroll on enroll.idStudent = member.idMember "
            + "inner join subject on subject.idsubject = enroll.idSubject";

//    public List<Subject> getAllSubjects() {
//
//        db = new DBConnection();
//        List<Subject> subjects = new ArrayList<>();
//
//        try {
//            this.con = db.getConnection();
//            pstmt = con.prepareStatement(SELECT_SUBJECTS_QUERY);
//            rs = pstmt.executeQuery();
//
//            while (rs.next()) {
//                int idsubject = rs.getInt(1);
//                int idteacher = rs.getInt(2);
//                String name = rs.getString(3);
//
//                subjects.add(new Subject(idsubject, idteacher, name));
//            }
//
//        } catch (Exception ex) {
//            Logger.getLogger(SubjectJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//        return subjects;
//    }
//
//    public boolean addSubject(Subject subject) {
//        db = new DBConnection();
//        boolean succes = false;
//
//        try {
//            this.con = db.getConnection();
//
//            pstmt = con.prepareStatement("insert into subject "
//                    + "(idteacher,name) "
//                    + "values (?,?)");
//
//            pstmt.setInt(1, subject.getIdTeacher());
//            pstmt.setString(2, subject.getName());
//            pstmt.executeUpdate();
//            succes = true;
//        } catch (Exception e) {
//            Logger.getLogger(SubjectJDBCDAO.class.getName()).log(Level.SEVERE, null, e);
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//        return succes;
//    }
//
//    public boolean deleteSubject(Subject subject) {
//        db = new DBConnection();
//        boolean succes = false;
//        try {
//
//            this.con = db.getConnection();
//            con.setAutoCommit(false);
//            pstmt = con.prepareStatement("select * from `subject`"
//                    + " inner join enroll on enroll.idSubject = subject.idsubject "
//                    + "inner join student on student.idstudent = enroll.idStudent");
//            rs = pstmt.executeQuery();
//            if (rs.next() == false) {
//
//                pstmt = con.prepareStatement("delete from `subject` where idsubject = ?");
//
//                pstmt.setInt(1,
//                        subject.getIdSubject());
//                pstmt.executeUpdate();
//
//                con.commit();
//
//                succes = true;
//            } else {
//                con.commit();
//
//                succes = false;
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(SubjectJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                con.setAutoCommit(true);
//            } catch (SQLException ex) {
//                Logger.getLogger(SubjectJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//        return succes;
//
//    }
//
//    public List<Student> getStudentEnrolled(int idSubject) {
//        db = new DBConnection();
//        List<Student> students = new ArrayList<>();
//
//        try {
//            this.con = db.getConnection();
//            pstmt = con.prepareStatement(SELECT_STUDENTS_QUERY);
//            rs = pstmt.executeQuery();
//
//            pstmt = con.prepareStatement(SELECT_MEMBER_STUDENTS_QUERY);
//            rs1 = pstmt.executeQuery();
//
//            while (rs.next()) {
//                rs1.next();
//                int idstudent = rs.getInt(1);
//                String schoolYear = rs.getString(2);
//                String nif = rs1.getString(2);
//                String name = rs1.getString(3);
//                String address = rs1.getString(4);
//                String phone = rs1.getString(5);
//                Date dateOfBirth = rs1.getDate(6);
//                String idUser = rs1.getString(7);
//
//                students.add(new Student(schoolYear, idstudent, nif, name,
//                        address, phone, LocalDate.parse(dateOfBirth.toString()), idUser));
//            }
//
//        } catch (Exception ex) {
//            Logger.getLogger(SubjectJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//        return students;
//    }
//
//    public boolean enrol(int idSubject, int idStudent) {
//
//        db = new DBConnection();
//        boolean succes = false;
//
//        try {
//            this.con = db.getConnection();
//
//            pstmt = con.prepareStatement("insert into enroll "
//                    + "(idStudent,idSubject) "
//                    + "values (?,?)");
//
//            pstmt.setInt(1, idStudent);
//            pstmt.setInt(2, idSubject);
//            pstmt.executeUpdate();
//            succes = true;
//        } catch (Exception e) {
//            Logger.getLogger(SubjectJDBCDAO.class.getName()).log(Level.SEVERE, null, e);
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//        return succes;
//    }
//
//    public boolean updateSubject(Subject subject) {
//        db = new DBConnection();
//        boolean succes = false;
//
//        try {
//            this.con = db.getConnection();
//
//            pstmt = con.prepareStatement("update subject "
//                    + "set idTeacher = ?, name = ? where idsubject = ? ");
//
//            pstmt.setInt(1, subject.getIdTeacher());
//            pstmt.setString(2, subject.getName());
//            pstmt.setInt(3, subject.getIdSubject());
//            pstmt.executeUpdate();
//            succes = true;
//        } catch (Exception e) {
//            Logger.getLogger(SubjectJDBCDAO.class.getName()).log(Level.SEVERE, null, e);
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//        return succes;
//    }
//
//    public boolean deleteStudent(Student st, Subject sb) {
//        db = new DBConnection();
//        boolean succes = false;
//
//        try {
//            this.con = db.getConnection();
//
//            pstmt = con.prepareStatement("DELETE FROM `iesquevedo`.`enroll` WHERE enroll.idSubject = ? and enroll.idStudent = ?;");
//
//            pstmt.setInt(1, sb.getIdSubject());
//            pstmt.setInt(2, st.getIdMember());
//            pstmt.executeUpdate();
//            succes = true;
//        } catch (Exception e) {
//            Logger.getLogger(SubjectJDBCDAO.class.getName()).log(Level.SEVERE, null, e);
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//        return succes;
//    }
    @Override
    public Subject get(int id) {
        Subject s = new Subject(id, id, SELECT_STUDENTS_QUERY);
        return s;
    }

    @Override
    public List<Subject> getAll() {
        List<Subject> l = new ArrayList<>();
        return l;
    }

    @Override
    public void save(Subject t) {
    }

    @Override
    public boolean update(Subject t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Subject t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean enroll(int idStudent, int idSubject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean deleteStudent(Student st, Subject sb) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Student> getStudentsEnrolled(int idSubject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Subject> getSubjectsFromStudent(Student st) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}

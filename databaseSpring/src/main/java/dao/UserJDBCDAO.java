/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author CAR
 */
public class UserJDBCDAO {

    private ResultSet rs;
    private PreparedStatement pstmt;
    private DBConnection db;
    private Connection con;

    public List<User> getAllUsers() {
        db = new DBConnection();
        List<User> users = new ArrayList<>();

        try {
            this.con = db.getConnection();
            pstmt = con.prepareStatement("select * from user");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                String name = rs.getString(1);
                String password = rs.getString(2);
                int idTypeOfUser = rs.getInt(3);
           

                users.add(new User(name, password, idTypeOfUser));
            }

        } catch (Exception ex) {
            Logger.getLogger(UserJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            db.releaseResource(rs);
            db.releaseResource(pstmt);
            db.closeConnection(con);
        }
        return users;
    }

}

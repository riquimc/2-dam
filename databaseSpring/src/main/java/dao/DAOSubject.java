/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Student;
import model.Subject;

/**
 *
 * @author dam2
 */
public interface DAOSubject {

    Subject get(int id);

    List<Subject> getAll();

    void save(Subject t);

    boolean update(Subject t);

    boolean delete(Subject t);

    boolean enroll(int idStudent, int idSubject);

    boolean deleteStudent(Student st, Subject sb);

    List<Student> getStudentsEnrolled(int idSubject);

    List<Subject> getSubjectsFromStudent(Student st);

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import model.Subject;
import model.Teacher;
import model.User;

/**
 *
 * @author dam2
 */
public interface DAOTeacher {

    Teacher get(User u);

    List<Teacher> getAll();

    void save(Teacher t);

    boolean update(Teacher t, Teacher oldTeacher);

    boolean delete(Teacher t);

    boolean markStudent(int mark, int numberCall, int idStudent, int idSubject);
    
    List<Subject> loadSubjectsFromTeacher(User u);
        
    
}

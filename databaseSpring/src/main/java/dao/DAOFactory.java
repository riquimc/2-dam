/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

/**
 *
 * @author dam2
 */
public class DAOFactory {

    private static DAOFactory instance;
    private String sourceCoffee;
    private Properties daoProps;

    private static final String PROPERTIES_FILE = "config/dao-properties.xml";

    public DAOFactory() {
        try {
            setDAOType(PROPERTIES_FILE);
        } catch (Exception e) {
        }
    }

    private void setDAOType(String configFile) throws IOException, InvalidPropertiesFormatException {
        daoProps = new Properties();
        daoProps.loadFromXML(Files.newInputStream(Paths.get(configFile)));
    }

    public DAOStudent getDAOIesQuevedo() {
        StudentsDAO dao = null;
        sourceCoffee = daoProps.getProperty("daoIesQuevedo");
        //Un if por cada tipo de DAO posible a instanciar
        if (sourceCoffee.equals("DAOJDBCIesQuevedo")) {
//            dao = new DAOJDBCCoffee(); 
        } else if (sourceCoffee.equals("DAOXMLIesQuevedo")) {
            // dao= new DAOXMLCoffee();
        } else if (sourceCoffee.equals("DAODBUtilsIesQuevedo")) {
             dao= new StudentsDAO();
        }

        return dao;
    }
}

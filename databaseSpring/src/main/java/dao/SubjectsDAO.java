/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Student;
import model.Subject;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

/**
 *
 * @author dam2
 */
public class SubjectsDAO implements DAOSubject {

    private static final String SELECT_SUBJECTS = "select * from subject";

    @Override
    public Subject get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Subject> getAll() {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query(SELECT_SUBJECTS, BeanPropertyRowMapper.newInstance(Subject.class));
    }

    @Override
    public void save(Subject t) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(
                DBConnPool.getInstance().getDataSource()).withTableName("subject")
                .usingGeneratedKeyColumns("idsubject");

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("idTeacher", t.getIdTeacher());
        parameters.put("name", t.getName());

        jdbcInsert.execute(parameters);

    }

    @Override
    public boolean update(Subject t) {
        boolean ok = true;
        try {
            JdbcTemplate jtm = new JdbcTemplate(
                    DBConnPool.getInstance().getDataSource());
            jtm.update("update subject set idTeacher=?, name=? where idsubject=?", t.getIdTeacher(), t.getName(), t.getIdsubject());
        } catch (Exception e) {
            ok = false;
            throw e;
        }
        return ok;
    }

    @Override
    public boolean delete(Subject t) {
        boolean ok = true;
        try {
            JdbcTemplate jtm = new JdbcTemplate(
                    DBConnPool.getInstance().getDataSource());
            jtm.update("delete from subject where idsubject=?", t.getIdsubject());
        } catch (Exception e) {
            ok = false;
            throw e;
        }
        return ok;
    }

    @Override
    public boolean enroll(int idStudent, int idSubject) {
        boolean ok = true;
        try {
            SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(
                    DBConnPool.getInstance().getDataSource()).withTableName("enroll")
                    .usingGeneratedKeyColumns("idenroll");

            Map<String, Object> parameters = new HashMap<>();

            parameters.put("idStudent", idStudent);
            parameters.put("idSubject", idSubject);
            parameters.put("NumberCalls", null);
            parameters.put("Mark", null);
            jdbcInsert.execute(parameters);
        } catch (Exception e) {
            ok = false;
            throw e;
        }
        return ok;

    }

    @Override
    public boolean deleteStudent(Student st, Subject sb) {

        boolean ok = true;
        try {
            JdbcTemplate jtm = new JdbcTemplate(
                    DBConnPool.getInstance().getDataSource());
            jtm.update("delete from enroll where idStudent=? and idSubject=?", st.getIdMember(), sb.getIdsubject());
        } catch (Exception e) {
            ok = false;
            throw e;
        }
        return ok;
    }

    @Override
    public List<Student> getStudentsEnrolled(int idSubject) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query("select * from student inner join `member` on `member`.idMember = student.idMember "
                + "inner join user on user.iduser = `member`.iduser "
                + "inner join enroll on enroll.idStudent= student.idMember "
                + "where enroll.idSubject=" + idSubject,
                BeanPropertyRowMapper.newInstance(Student.class));
    }

    @Override
    public List<Subject> getSubjectsFromStudent(Student st) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query("select * from subject inner join `enroll` on `enroll`.idSubject = subject.idsubject "
                + "inner join student on student.idMember = `enroll`.idStudent "
                + "where student.idMember=" + st.getIdMember(), BeanPropertyRowMapper.newInstance(Subject.class));
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import config.ConfigurationSql;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 *
 * @author dam2
 */
public class DBConnPool {

    public static DBConnPool dbConnectionPool = null;
    public BasicDataSource pool = null;
    public BasicDataSource dataSource = null;
    public String driver;
    public String urlDB;
    public String userName;
    public String password;

    private DBConnPool() {
        super();
        pool = this.getPool();
    }

    public static DBConnPool getInstance() {
        if (dbConnectionPool == null) {
            dbConnectionPool = new DBConnPool();
        }

        return dbConnectionPool;
    }

    /**
     * Creates connection dbConnectionPool
     */
    private BasicDataSource getPool() {

        driver = ConfigurationSql.getInstance().getProperty("driver");
        urlDB = ConfigurationSql.getInstance().getProperty("urlDB");
        userName = ConfigurationSql.getInstance().getProperty("user_name");
        password = ConfigurationSql.getInstance().getProperty("password");

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(driver);
        basicDataSource.setUsername(userName);
        basicDataSource.setPassword(password);
        basicDataSource.setUrl(urlDB);

        basicDataSource.setInitialSize(4);

        return basicDataSource;
    }

    /**
     * Closes dbConnectionPool
     *
     * @param dbConnectionPool
     * @throws java.sql.SQLException
     */
    public static void closePool(BasicDataSource dbConnectionPool) throws SQLException {
        if (dbConnectionPool != null) {
            dbConnectionPool.close();
            dbConnectionPool = null;
        }
    }

    public Connection getConnection() throws SQLException {
        return pool.getConnection();
    }

    protected BasicDataSource getDataSource() {
        driver = ConfigurationSql.getInstance().getProperty("driver");
        urlDB = ConfigurationSql.getInstance().getProperty("urlDB");
        userName = ConfigurationSql.getInstance().getProperty("user_name");
        password = ConfigurationSql.getInstance().getProperty("password");

        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(driver);
        basicDataSource.setUsername(userName);
        basicDataSource.setPassword(password);
        basicDataSource.setUrl(urlDB);

        basicDataSource.setInitialSize(4);

        return basicDataSource;

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import model.Teacher;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Subject;
import model.User;

/**
 * @author Lucia
 */
public class TeacherJDBCDAO implements DAOTeacher {

    private ResultSet rs;
    private ResultSet rs1;
    private PreparedStatement pstmt;
    private DBConnection db;
    private Connection con;

    private static final String SELECT_TEACHERS_QUERY = "select * from teacher";
    private static final String SELECT_MEMBER_TEACHERS_QUERY = "select * from `member` inner join teacher on teacher.idteacher = `member`.IdMember";

//    public List<Teacher> getAllTeachers() {
//        db = new DBConnection();
//        List<Teacher> teachers = new ArrayList<>();
//
//        try {
//            this.con = db.getConnection();
//            pstmt = con.prepareStatement(SELECT_TEACHERS_QUERY);
//            rs = pstmt.executeQuery();
//            pstmt = con.prepareStatement(SELECT_MEMBER_TEACHERS_QUERY);
//            rs1 = pstmt.executeQuery();
//
//            while (rs.next()) {
//                rs1.next();
//                int idStudent = rs.getInt(1);
//                Date year = rs.getDate(2);
//                String nif = rs1.getString(2);
//                String name = rs1.getString(3);
//                String address = rs1.getString(4);
//                String phone = rs1.getString(5);
//                Date dateOfBirth = rs1.getDate(6);
//                String idUser = rs1.getString(7);
//
////                teachers.add(new Teacher(LocalDate.parse(year.toString()), idStudent, nif, name,
////                        address, phone, LocalDate.parse(dateOfBirth.toString()), idUser));
//            }
//
//        } catch (Exception ex) {
//            Logger.getLogger(TeacherJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//
//        } finally {
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.releaseResource(rs1);
//            db.closeConnection(con);
//        }
//        return teachers;
//    }
//
//    public boolean addTeacher(Teacher teacher, String password) {
//        db = new DBConnection();
//        boolean succes = false;
//
//        try {
//            this.con = db.getConnection();
//            con.setAutoCommit(false);
//
//            pstmt = con.prepareStatement("insert into user "
//                    + "(iduser,passwd,idTypeUser) "
//                    + "values (?,?,?)");
//
//            pstmt.setString(1, teacher.getIdUser());
//            pstmt.setString(2, password);
//            pstmt.setInt(3, 3);
//            pstmt.executeUpdate();
//
//            pstmt = con.prepareStatement("insert into `member` "
//                    + "(NIF,fullName,Address,phone,DateOfBirth,IdUser) "
//                    + "values (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
//
//            pstmt.setString(1, teacher.getNIF());
//            pstmt.setString(2, teacher.getFullName());
//            pstmt.setString(3, teacher.getAddress());
//            pstmt.setString(4, teacher.getPhone());
//            pstmt.setDate(5, java.sql.Date.valueOf(teacher.getDateOfBirth()));
//            pstmt.setString(6, teacher.getIdUser());
//
//            pstmt.executeUpdate();
//            rs = pstmt.getGeneratedKeys();
//
//            String key = null;
//
//            if (rs != null && rs.next()) {
//                key = rs.getString(1);
//            }
//
//            pstmt = con.prepareStatement("insert into teacher "
//                    + "(idteacher,startDate) "
//                    + "values (?,?)");
//
//            pstmt.setString(1, key);
//            pstmt.setDate(2, java.sql.Date.valueOf(teacher.getStartDate()));
//            pstmt.executeUpdate();
//
//            con.commit();
//            succes = true;
//        } catch (Exception ex) {
//            Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                con.setAutoCommit(true);
//            } catch (SQLException ex) {
//                Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//
//        return succes;
//    }
//
//    public boolean deleteTeacher(Teacher teacher) {
//        db = new DBConnection();
//        boolean succes = false;
//        try {
//
//            this.con = db.getConnection();
//            con.setAutoCommit(false);
//            pstmt = con.prepareStatement("select * from `teacher`"
//                    + " inner join subject on subject.idTeacher = teacher.idteacher");
//            rs = pstmt.executeQuery();
//            if (rs.next() == false) {
//
//                pstmt = con.prepareStatement("delete from `teacher` where idteacher = ?");
//
//                pstmt.setInt(1,
//                        teacher.getIdMember());
//                pstmt.executeUpdate();
//
//                pstmt = con.prepareStatement("delete from `member` where idMember = ?");
//
//                pstmt.setInt(1,
//                        teacher.getIdMember());
//                pstmt.executeUpdate();
//
//                pstmt = con.prepareStatement("delete from user where iduser = ?");
//
//                pstmt.setString(1,
//                        teacher.getIdUser());
//                pstmt.executeUpdate();
//
//                con.commit();
//                succes = true;
//            } else {
//                succes = false;
//                con.commit();
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                con.setAutoCommit(true);
//            } catch (SQLException ex) {
//                Logger.getLogger(StudentsJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//        return succes;
//
//    }
//
//    public boolean updateTeacher(Teacher teacher, String password) {
//        db = new DBConnection();
//        boolean succes = false;
//        try {
//
//            this.con = db.getConnection();
//            con.setAutoCommit(false);
//
//            pstmt = con.prepareStatement("update teacher "
//                    + "set startDate = ? where idteacher = ?");
//
//            pstmt.setDate(1,
//                    java.sql.Date.valueOf(teacher.getStartDate()));
//            pstmt.setInt(2, teacher.getIdMember());
//            pstmt.executeUpdate();
//
//            pstmt = con.prepareStatement("update `member` "
//                    + "set nif = ?, fullName = ?, address = ?, phone = ?, dateOfBirth = ?,idUser = ?"
//                    + " where idMember = ?");
//
//            pstmt.setString(1,
//                    teacher.getNIF());
//            pstmt.setString(2,
//                    teacher.getFullName());
//            pstmt.setString(3,
//                    teacher.getAddress());
//            pstmt.setString(4,
//                    teacher.getPhone());
//            pstmt.setDate(5,
//                    java.sql.Date.valueOf(teacher.getDateOfBirth()));
//            pstmt.setString(6,
//                    teacher.getIdUser());
//
//            pstmt.setInt(7, teacher.getIdMember());
//            pstmt.executeUpdate();
//
//            pstmt = con.prepareStatement("update user "
//                    + "set iduser = ?, passwd = ? where iduser = ?");
//            pstmt.setString(1, teacher.getIdUser());
//            pstmt.setString(2, password);
//            pstmt.setString(3, teacher.getIdUser());
//            pstmt.executeUpdate();
//
//            con.commit();
//            succes = true;
//        } catch (SQLException ex) {
//            Logger.getLogger(TeacherJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                con.setAutoCommit(true);
//            } catch (SQLException ex) {
//                Logger.getLogger(TeacherJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//
//        return succes;
//    }
//
//    public boolean markStudent(int mark, int numberCall, int idStudent, int idSubject) {
//        db = new DBConnection();
//        boolean succes = false;
//        try {
//
//            this.con = db.getConnection();
//            con.setAutoCommit(false);
//
//            pstmt = con.prepareStatement("insert into enroll "
//                    + "(idStudent,idSubject,NumberCalls,Mark) "
//                    + "values (?,?,?,?)");
//            pstmt.setInt(1, idStudent);
//            pstmt.setInt(2, idSubject);
//            pstmt.setInt(3, numberCall);
//            pstmt.setInt(4, mark);
//            pstmt.executeUpdate();
//
//            con.commit();
//            succes = true;
//        } catch (SQLException ex) {
//            Logger.getLogger(TeacherJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                con.setAutoCommit(true);
//            } catch (SQLException ex) {
//                Logger.getLogger(TeacherJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            db.releaseResource(rs);
//            db.releaseResource(pstmt);
//            db.closeConnection(con);
//        }
//
//        return succes;
//    }
    @Override
    public Teacher get(User u) {
        //Teacher t = new Teacher(LocalDate.MAX, SELECT_TEACHERS_QUERY, SELECT_TEACHERS_QUERY, id, id, SELECT_TEACHERS_QUERY, SELECT_TEACHERS_QUERY, SELECT_TEACHERS_QUERY, SELECT_TEACHERS_QUERY, LocalDate.MIN);

        return null;
    }

    @Override
    public List<Teacher> getAll() {
        List<Teacher> l = new ArrayList<>();
        return l;
    }

    @Override
    public void save(Teacher t) {
    }

    @Override
    public boolean update(Teacher t, Teacher oldTeacher) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Teacher t) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean markStudent(int mark, int numberCall, int idStudent, int idSubject) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Subject> loadSubjectsFromTeacher(User u) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

    

}

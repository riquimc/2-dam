/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Subject;
import model.Teacher;
import model.User;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 *
 * @author dam2
 */
public class TeachersDAO implements DAOTeacher {

    private static final String SELECT_TEACHERS = "select * from teacher inner join `member` on `member`.idMember = teacher.idMember "
            + "inner join user on user.iduser = `member`.iduser";

    @Override
    public Teacher get(User u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query("select * from teacher inner join `member` on `member`.idMember = teacher.idMember "
                + "inner join user on user.iduser = `member`.iduser "
                + "where user.iduser=" + u.getIduser(), BeanPropertyRowMapper.newInstance(Teacher.class)).get(0);
    }

    @Override
    public List<Teacher> getAll() {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query(SELECT_TEACHERS, BeanPropertyRowMapper.newInstance(Teacher.class));
    }

    @Override
    public void save(Teacher t) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(
                DBConnPool.getInstance().getDataSource()).withTableName("user");

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("iduser", t.getIduser());
        parameters.put("password", t.getPassword());
        parameters.put("idTypeUser", t.getIdTypeUser());

        jdbcInsert.execute(parameters);

        jdbcInsert = new SimpleJdbcInsert(
                DBConnPool.getInstance().getDataSource()).withTableName("`member`")
                .usingGeneratedKeyColumns("idMember");
        parameters.clear();

        parameters.put("nif", t.getNif());
        parameters.put("fullName", t.getFullName());
        parameters.put("address", t.getAddress());
        parameters.put("phone", t.getPhone());
        parameters.put("dateOfBirth", t.getDateOfBirth());
        parameters.put("idUser", t.getIduser());

        int key = jdbcInsert.executeAndReturnKey(parameters).intValue();

        jdbcInsert = new SimpleJdbcInsert(
                DBConnPool.getInstance().getDataSource()).withTableName("teacher");
        parameters.clear();

        parameters.put("idMember", key);
        parameters.put("startDate", t.getStartDate());
        jdbcInsert.execute(parameters);
    }

    @Override
    public boolean update(Teacher t, Teacher oldTeacher) {
        boolean ok = true;

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(DBConnPool.getInstance().getDataSource());
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        JdbcTemplate jtm = new JdbcTemplate(
                DBConnPool.getInstance().getDataSource());
        try {
            jtm.update("update user set iduser=?, password=?, idTypeUser=? where iduser = ?",
                    t.getIduser(), t.getPassword(), t.getIdTypeUser(), oldTeacher.getIduser());

            jtm.update("update `member` set nif = ?, fullName=?, address=?, phone=?, dateOfBirth=?, idUser=? where idMember = ?",
                    t.getNif(), t.getFullName(), t.getAddress(), t.getPhone(), t.getDateOfBirth(), t.getIduser(), oldTeacher.getIdMember());
            jtm.update("update teacher set startDate=? where idMember = ?", t.getStartDate(), oldTeacher.getIdMember());
            transactionManager.commit(txStatus);
        } catch (DataIntegrityViolationException e) {
            e.getStackTrace();
            try {
                if (e.getMessage().equals("IntegrityConstraintViolation")) {

                    SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(
                            DBConnPool.getInstance().getDataSource()).withTableName("user");

                    Map<String, Object> parameters = new HashMap<>();

                    parameters.put("iduser", t.getIduser());
                    parameters.put("password", t.getPassword());
                    parameters.put("idTypeUser", t.getIdTypeUser());

                    jdbcInsert.execute(parameters);

                    jtm.update("update `member` set nif = ?, fullName=?, address=?, phone=?, dateOfBirth=?, idUser=? where idMember = ?",
                            t.getNif(), t.getFullName(), t.getAddress(), t.getPhone(), t.getDateOfBirth(), t.getIduser(), oldTeacher.getIdMember());
                    jtm.update("update teacher set startDate=? where idMember = ?", t.getStartDate(), oldTeacher.getIdMember());
                    jtm.update("delete from user where iduser = ?", oldTeacher.getIduser());
                    transactionManager.commit(txStatus);
                }
            } catch (Exception ex) {
                ok = false;
                transactionManager.rollback(txStatus);
                throw ex;
            }
        } catch (DataAccessException | TransactionException ex) {
            ok = false;
            transactionManager.rollback(txStatus);
            throw ex;
        }

        return ok;
    }

    @Override
    public boolean delete(Teacher t) {
        boolean ok = false;

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(DBConnPool.getInstance().getDataSource());
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        try {
            JdbcTemplate jtm = new JdbcTemplate(
                    transactionManager.getDataSource());
            jtm.update("delete from `subject` where idTeacher = ?", t.getIdMember());
            jtm.update("delete from teacher where idMember = ?", t.getIdMember());
            jtm.update("delete from `member` where idMember = ?", t.getIdMember());
            jtm.update("delete from user where iduser = ?", t.getIduser());

            transactionManager.commit(txStatus);
            ok = true;

        } catch (Exception e) {
            transactionManager.rollback(txStatus);
            throw e;
        }
        return ok;
    }

    @Override
    public boolean markStudent(int mark, int numberCall, int idStudent, int idSubject) {
        boolean ok = true;
        try {

            JdbcTemplate jtm = new JdbcTemplate(
                    DBConnPool.getInstance().getDataSource());
            List<Integer> list = null;

            if (numberCall != 1) {
                list = jtm.query("select Mark from enroll where NumberCalls=" + (numberCall - 1) + " and idSubject=" + idSubject + " and idStudent=" + idStudent,
                        new RowMapper<Integer>() {
                    @Override
                    public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                        Integer mark = rs.getInt("Mark");
                        return mark;
                    }

                });

            }
            if ((null == list && numberCall == 1) || !list.isEmpty()) {
                if ((null != list
                        && list.get(0) < 5) || numberCall == 1) {
                    if (numberCall == 1) {

                        jtm.update("update enroll set NumberCalls=?, Mark=? where idSubject=? and idStudent=? ",
                                numberCall, mark, idSubject, idStudent);
                    } else {
                        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(
                                DBConnPool.getInstance().getDataSource()).withTableName("enroll")
                                .usingGeneratedKeyColumns("idenroll");

                        Map<String, Object> parameters = new HashMap<>();

                        parameters.put("idStudent", idStudent);
                        parameters.put("idSubject", idSubject);
                        parameters.put("NumberCalls", idSubject);
                        parameters.put("Mark", mark);

                        jdbcInsert.execute(parameters);
                    }
                } else {
                    ok = false;
                }
            } else {
                ok = false;
            }
        } catch (Exception e) {
            ok = false;
            throw e;
        }
        return ok;
    }

    @Override
    public List<Subject> loadSubjectsFromTeacher(User u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query("select * from subject "
                + "inner join teacher on teacher.idMember = subject.idTeacher "
                + "inner join `member` on `member`.idMember = teacher.idMember "
                + "inner join user on user.iduser = `member`.idUser "
                + "where user.iduser =" + u.getIduser(), BeanPropertyRowMapper.newInstance(Subject.class));
    }

}

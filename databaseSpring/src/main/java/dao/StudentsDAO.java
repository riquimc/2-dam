/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Student;
import model.User;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 *
 * @author dam2
 */
public class StudentsDAO implements DAOStudent {

    private static final String SELECT_STUDENTS = "select * from student inner join `member` on `member`.idMember = student.idMember "
            + "inner join user on user.iduser = `member`.iduser";

    @Override
    public Student get(User u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query("select * from student inner join `member` on `member`.idMember = student.idMember "
                + "inner join user on user.iduser = `member`.iduser "
                + "where user.iduser=" + u.getIduser(), BeanPropertyRowMapper.newInstance(Student.class)).get(0);

    }

    @Override
    public List<Student> getAll() {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query(SELECT_STUDENTS, BeanPropertyRowMapper.newInstance(Student.class));
    }

    @Override
    public void save(Student t) {
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(
                DBConnPool.getInstance().getDataSource()).withTableName("user");

        Map<String, Object> parameters = new HashMap<>();

        parameters.put("iduser", t.getIduser());
        parameters.put("password", t.getPassword());
        parameters.put("idTypeUser", t.getIdTypeUser());

        jdbcInsert.execute(parameters);

        jdbcInsert = new SimpleJdbcInsert(
                DBConnPool.getInstance().getDataSource()).withTableName("`member`")
                .usingGeneratedKeyColumns("idMember");
        parameters.clear();

        parameters.put("nif", t.getNif());
        parameters.put("fullName", t.getFullName());
        parameters.put("address", t.getAddress());
        parameters.put("phone", t.getPhone());
        parameters.put("dateOfBirth", t.getDateOfBirth());
        parameters.put("idUser", t.getIduser());

        int key = jdbcInsert.executeAndReturnKey(parameters).intValue();

        jdbcInsert = new SimpleJdbcInsert(
                DBConnPool.getInstance().getDataSource()).withTableName("student");
        parameters.clear();

        parameters.put("idMember", key);
        parameters.put("schoolYear", t.getSchoolYear());
        jdbcInsert.execute(parameters);
    }

    @Override
    public boolean update(Student t, Student oldStudent) {
        boolean ok = true;

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(DBConnPool.getInstance().getDataSource());
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        JdbcTemplate jtm = new JdbcTemplate(
                DBConnPool.getInstance().getDataSource());
        try {
            jtm.update("update user set iduser=?, password=?, idTypeUser=? where iduser = ?",
                    t.getIduser(), t.getPassword(), t.getIdTypeUser(), oldStudent.getIduser());

            jtm.update("update `member` set nif = ?, fullName=?, address=?, phone=?, dateOfBirth=?, idUser=? where idMember = ?",
                    t.getNif(), t.getFullName(), t.getAddress(), t.getPhone(), t.getDateOfBirth(), t.getIduser(), oldStudent.getIdMember());
            jtm.update("update student set schoolYear=? where idMember = ?", t.getSchoolYear(), oldStudent.getIdMember());
            transactionManager.commit(txStatus);
        } catch (DataIntegrityViolationException e) {
            e.getStackTrace();
            try {
                if (e.getMessage().equals("IntegrityConstraintViolation")) {

                    SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(
                            DBConnPool.getInstance().getDataSource()).withTableName("user");

                    Map<String, Object> parameters = new HashMap<>();

                    parameters.put("iduser", t.getIduser());
                    parameters.put("password", t.getPassword());
                    parameters.put("idTypeUser", t.getIdTypeUser());

                    jdbcInsert.execute(parameters);

                    jtm.update("update `member` set nif = ?, fullName=?, address=?, phone=?, dateOfBirth=?, idUser=? where idMember = ?",
                            t.getNif(), t.getFullName(), t.getAddress(), t.getPhone(), t.getDateOfBirth(), t.getIduser(), oldStudent.getIdMember());
                    jtm.update("update student set schoolYear=? where idMember = ?", t.getSchoolYear(), oldStudent.getIdMember());
                    jtm.update("delete from user where iduser = ?", oldStudent.getIduser());
                    transactionManager.commit(txStatus);
                }
            } catch (Exception ex) {
                ok = false;
                transactionManager.rollback(txStatus);
                throw ex;
            }
        } catch (DataAccessException | TransactionException ex) {
            ok = false;
            transactionManager.rollback(txStatus);
            throw ex;
        }

        return ok;
    }

    @Override
    public boolean delete(Student t) {
        boolean ok = false;

        TransactionDefinition txDef = new DefaultTransactionDefinition();
        DataSourceTransactionManager transactionManager = new DataSourceTransactionManager(DBConnPool.getInstance().getDataSource());
        TransactionStatus txStatus = transactionManager.getTransaction(txDef);

        try {
            JdbcTemplate jtm = new JdbcTemplate(
                    transactionManager.getDataSource());
            jtm.update("delete from `enroll` where idStudent = ?", t.getIdMember());
            jtm.update("delete from student where idMember = ?", t.getIdMember());
            jtm.update("delete from `member` where idMember = ?", t.getIdMember());
            jtm.update("delete from user where iduser = ?", t.getIduser());

            transactionManager.commit(txStatus);
            ok = true;

        } catch (Exception e) {
            transactionManager.rollback(txStatus);
            throw e;
        }
        return ok;
    }

    @Override
    public List<Integer> getMarks(int idStudent, int idSubject) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query("select Mark from enroll "
                + "where enroll.idSubject=" + idSubject + " and enroll.idStudent=" + idStudent,
                new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                Integer mark = rs.getInt("Mark");
                return mark;
            }

        });

    }

}

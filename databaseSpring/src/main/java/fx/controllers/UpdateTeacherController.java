/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import model.Teacher;
import services.TeacherServices;

/**
 * FXML Controller class
 *
 * @author CAR
 */
public class UpdateTeacherController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Alert alert;
    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }
    @FXML
    private ComboBox fxTeachers;

    public ComboBox getFxTeachers() {
        return fxTeachers;
    }

    @FXML
    private TextField fxNif;
    @FXML
    private TextField fxName;
    @FXML
    private TextField fxPhone;
    @FXML
    private DatePicker fxDoB;
    @FXML
    private TextField fxAddress;
    @FXML
    private TextField fxUserName;
    @FXML
    private TextField fxPassword;

    private Teacher oldTeacher;
    public void cleanValues() {
        fxNif.clear();
        fxName.clear();
        fxPhone.clear();
        fxDoB.setValue(null);
        fxAddress.clear();
        fxPassword.clear();
        fxUserName.clear();
    }

    public boolean checkEmptyFields() {
        return fxNif.getText().isEmpty() || fxName.getText().isEmpty()
                || fxPhone.getText().isEmpty() || fxDoB.getValue() == null
                || fxAddress.getText().isEmpty() || fxUserName.getText().isEmpty()
                || fxPassword.getText().isEmpty();
    }

    public void loadTeachers() {
        fxTeachers.getItems().clear();
        TeacherServices ts = new TeacherServices();
        fxTeachers.getItems().addAll(ts.getAllTeachers());

    }

    @FXML
    private void updateTeacher() {
        TeacherServices ts = new TeacherServices();

        if (checkEmptyFields()) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please fill all data");
            alert.showAndWait();
        } else {
            if (ts.updateTeacher(fxNif.getText(), fxName.getText(), fxPhone.getText(), fxDoB.getValue(), fxAddress.getText(), fxUserName.getText(), LocalDate.now(),
                    fxPassword.getText(),oldTeacher)) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Teacher update");
                alert.showAndWait();
            } else {
                alert.setContentText("An error has occured please try later");
                alert.showAndWait();
            }
        }
        cleanValues();
    }

    @FXML
    private void showTeacher() {
        Teacher teacher = (Teacher) fxTeachers.getSelectionModel().getSelectedItem();
        fxNif.setText(teacher.getNif());
        fxName.setText(teacher.getFullName());
        fxPhone.setText(teacher.getPhone());
        fxDoB.setValue(teacher.getDateOfBirth());
        fxAddress.setText(teacher.getAddress());
        fxUserName.setText(teacher.getIduser());
        fxPassword.setText(teacher.getPassword());
        
        oldTeacher= teacher;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

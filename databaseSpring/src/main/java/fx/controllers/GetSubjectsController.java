/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import services.SubjectServices;

/**
 * FXML Controller class
 *
 * @author CAR
 */
public class GetSubjectsController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }

    @FXML
    private ListView fxListSubject;

    public void loadSubjects() {
        fxListSubject.getItems().clear();
        SubjectServices ss = new SubjectServices();
        fxListSubject.getItems().addAll(ss.getAllSubjects());
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

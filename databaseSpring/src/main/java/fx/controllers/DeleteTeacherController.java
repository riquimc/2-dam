/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import model.Teacher;
import services.TeacherServices;

/**
 * FXML Controller class
 *
 * @author dam2
 */
public class DeleteTeacherController implements Initializable {

    /**
     * Initializes the controller class.
     */
    PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }
    @FXML
    private ListView fxListDelete;

    @FXML
    public void fxDelete() {
        Teacher teacher = (Teacher) fxListDelete.getSelectionModel().getSelectedItem();
        TeacherServices tt = new TeacherServices();
        if (tt.deleteTeacher(teacher)) {
            Alert alert3 = new Alert(Alert.AlertType.INFORMATION);
            alert3.setHeaderText(null);
            alert3.setContentText("Teacher has been remove succesfully!");
            alert3.showAndWait();
        } else {
            Alert alert3 = new Alert(Alert.AlertType.ERROR);
            alert3.setHeaderText(null);
            alert3.setContentText("An error has ocurred, please try later");
            alert3.showAndWait();
        }
    }

    public void loadTeachers() {
        fxListDelete.getItems().clear();
        TeacherServices cs = new TeacherServices();
        List<Teacher> teachers;

        teachers = cs.getAllTeachers();

        if (teachers != null) {
            fxListDelete.getItems().addAll(teachers);
        } else {
            Alert alert3 = new Alert(Alert.AlertType.ERROR);
            alert3.setHeaderText(null);
            alert3.setContentText("Teachers file couldn`t be loaded");
            alert3.showAndWait();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

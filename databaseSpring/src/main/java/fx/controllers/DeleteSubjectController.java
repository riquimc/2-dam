/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import model.Subject;
import services.SubjectServices;

/**
 * FXML Controller class
 *
 * @author CAR
 */
public class DeleteSubjectController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }

    @FXML
    private ListView fxListSubjects;

    @FXML
    private void fxDelete() {
        Subject subject = (Subject) fxListSubjects.getSelectionModel().getSelectedItem();
        SubjectServices ss = new SubjectServices();
        if (ss.deleteSubject(subject)) {
            Alert alert3 = new Alert(Alert.AlertType.INFORMATION);
            alert3.setHeaderText(null);
            alert3.setContentText("Subject has been remove succesfully!");
            alert3.showAndWait();
        } else {
            Alert alert3 = new Alert(Alert.AlertType.ERROR);
            alert3.setHeaderText(null);
            alert3.setContentText("An error has ocurred, please try later");
            alert3.showAndWait();
        }
    }

    public void loadSubjects() {
        fxListSubjects.getItems().clear();
        SubjectServices ss = new SubjectServices();
        fxListSubjects.getItems().addAll(ss.getAllSubjects());
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import model.Student;
import model.Subject;
import model.User;
import services.StudentServices;
import services.SubjectServices;
import services.TeacherServices;

/**
 * FXML Controller class
 *
 * @author CAR
 */
public class MarkController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Alert alert;
    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }
    @FXML
    private ComboBox fxStudents;
    @FXML
    private ComboBox fxSubjects;
    @FXML
    private ComboBox fxCalls;
    @FXML
    private TextField fxMark;

    @FXML
    private void fxMarkStudent() {
        TeacherServices ts = new TeacherServices();
        int calls = ((Integer) fxCalls.getSelectionModel().getSelectedItem());
        int idStudent = ((Student) fxStudents.getSelectionModel().getSelectedItem()).getIdMember();
        int idSubject = ((Subject) fxSubjects.getSelectionModel().getSelectedItem()).getIdsubject();

        if (fxMark.getText().isEmpty()) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please fill all data");
            alert.showAndWait();
        } else {
            if (ts.markStudent(Integer.parseInt(fxMark.getText()), calls, idStudent, idSubject)) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Student update");
                alert.showAndWait();
            } else {
                alert.setContentText("You can't mark this call, please choose another one");
                alert.showAndWait();
            }
        }

    }

    @FXML
    public void loadSubjectsStudent() {
        Subject sub = (Subject) fxSubjects.getSelectionModel().getSelectedItem();
        SubjectServices ss = new SubjectServices();
        fxStudents.getItems().addAll(ss.getStudentsEnrolled(sub.getIdsubject()));

    }

    public void loadCombos(User u) {
        fxSubjects.getItems().clear();
        fxCalls.getItems().clear();

        TeacherServices ts = new TeacherServices();
        if (u.getIdTypeUser() == 3) {
            fxSubjects.getItems().addAll(ts.loadSubjectsFromTeacher(u));
            fxCalls.getItems().addAll(1, 2, 3, 4);
        } else if (u.getIdTypeUser() == 1) {
            SubjectServices ss = new SubjectServices();
            fxSubjects.getItems().addAll(ss.getAllSubjects());
            fxCalls.getItems().addAll(1, 2, 3, 4);
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

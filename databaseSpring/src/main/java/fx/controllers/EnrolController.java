/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import model.Student;
import model.Subject;
import services.StudentServices;
import services.SubjectServices;

/**
 * FXML Controller class
 *
 * @author CAR
 */
public class EnrolController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Alert alert;
    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }

    @FXML
    private ComboBox fxComboSubjects;

    @FXML
    private ListView fxListStudents;

    @FXML
    private ListView fxListStudentsSubject;

    public boolean checkEmptyFields(Subject subject, Student student) {
        return null == subject || null == student;
    }

    @FXML
    private void fxAddStudent() {
        SubjectServices ss = new SubjectServices();

        Subject subject = (Subject) fxComboSubjects.getSelectionModel().getSelectedItem();
        Student student = (Student) fxListStudents.getSelectionModel().getSelectedItem();

        if (checkEmptyFields(subject, student)) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please fill all data");
            alert.showAndWait();
        } else {
            if (ss.enrol(subject.getIdsubject(), student.getIdMember())) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Student enrolled");
                alert.showAndWait();
                loadStudents();
            } else {
                alert.setContentText("Student has already been enrolled in this subject");
                alert.showAndWait();
            }
        }
    }

    @FXML
    private void loadStudents() {
        fxListStudentsSubject.getItems().clear();
        SubjectServices ss = new SubjectServices();
        Subject subject = (Subject) fxComboSubjects.getSelectionModel().getSelectedItem();
        if (null != ss.getStudentsEnrolled(subject.getIdsubject())) {
            fxListStudentsSubject.getItems().addAll(ss.getStudentsEnrolled(subject.getIdsubject()));
        }
    }

    @FXML
    private void fxDeleteStudent() {
        SubjectServices ss = new SubjectServices();

        Subject subject = (Subject) fxComboSubjects.getSelectionModel().getSelectedItem();
        Student st = (Student) fxListStudentsSubject.getSelectionModel().getSelectedItem();
        if (ss.deleteStudent(st, subject)) {
            alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Student delete");
            alert.showAndWait();
            loadStudents();
        } else {
            alert.setContentText("There's been a problem try later");
            alert.showAndWait();
        }
    }

    public void loadListCombo() {
        fxComboSubjects.getItems().clear();
        fxListStudents.getItems().clear();

        SubjectServices ss = new SubjectServices();
        StudentServices StudentS = new StudentServices();

        fxComboSubjects.getItems().addAll(ss.getAllSubjects());
        fxListStudents.getItems().addAll(StudentS.getAllStudents());
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

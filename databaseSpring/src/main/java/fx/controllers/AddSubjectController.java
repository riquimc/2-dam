/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import model.Subject;
import model.Teacher;
import services.SubjectServices;
import services.TeacherServices;

/**
 * FXML Controller class
 *
 * @author CAR
 */
public class AddSubjectController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Alert alert;
    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }

    @FXML
    private TextField fxNameSubject;
    @FXML
    private ComboBox fxChooseTeacher;

    private boolean checkEmpty(Teacher teacher) {
        return fxNameSubject.getText().isEmpty()
                || null == teacher;
    }

    @FXML
    private void fxAddSubject() {
        Teacher teacher = (Teacher) fxChooseTeacher.getSelectionModel().getSelectedItem();
        SubjectServices ss = new SubjectServices();

        if (checkEmpty(teacher)) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please fill all data");
            alert.showAndWait();
        } else {
            if (ss.addSubject(fxNameSubject.getText(), 0, teacher.getIdMember())) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText(" subject Updated");
                alert.showAndWait();
            } else {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("An error has occured please try later");
                alert.showAndWait();
            }
        }
    }

    public void loadCombos() {
        fxChooseTeacher.getItems().clear();
        TeacherServices teacherS = new TeacherServices();
        fxChooseTeacher.getItems().addAll(teacherS.getAllTeachers());
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import model.Student;
import model.Subject;
import model.User;
import services.StudentServices;
import services.SubjectServices;

/**
 * FXML Controller class
 *
 * @author CAR
 */
public class GetMarksController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }
    @FXML
    private ComboBox fxStudents;
    @FXML
    private ComboBox fxSubjects;
    @FXML
    private Label fx1;

    public ComboBox getFxStudents() {
        return fxStudents;
    }

    @FXML
    private void fxGetMarks() {
        StudentServices StudentS = new StudentServices();
        Student st = (Student) fxStudents.getSelectionModel().getSelectedItem();
        Subject sb = (Subject) fxSubjects.getSelectionModel().getSelectedItem();

        fx1.setText(StudentS.getMarks(st.getIdMember(), sb.getIdsubject()).stream().map(arg0
                -> "-" + arg0 + "\n"
        ).collect(Collectors.joining()));
    }

    public void loadCombos(User u) {
        fx1.setText("");
        fxStudents.getItems().clear();
        fxSubjects.getItems().clear();

        SubjectServices ss = new SubjectServices();
        StudentServices StudentS = new StudentServices();

        if (u.getIdTypeUser() == 2) {
            Student st = StudentS.getStudent(u);
            fxStudents.getItems().addAll(st);
            fxSubjects.getItems().addAll(ss.getSubjectsFromStudents(st));
        } else if (u.getIdTypeUser() == 1) {
            fxSubjects.getItems().addAll(ss.getAllSubjects());
            fxStudents.getItems().addAll(StudentS.getAllStudents());
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import model.Student;
import services.StudentServices;

/**
 * FXML Controller class
 *
 * @author CAR
 */
public class DeleteStudentController implements Initializable {

    /**
     * Initializes the controller class.
     */
    PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }
    @FXML
    private ListView fxListDelete;

    @FXML
    private void fxDelete() {
        Student student = (Student) fxListDelete.getSelectionModel().getSelectedItem();
        StudentServices ss = new StudentServices();

        if (ss.checkStudentHasSubjects(student)) {
            Alert alert3 = new Alert(Alert.AlertType.CONFIRMATION);
            alert3.setHeaderText(null);
            alert3.setContentText("this student has Subjects, Dou you want to remove it anyway?");
            Optional<ButtonType> result = alert3.showAndWait();
            if (result.get() == ButtonType.OK) {
                if (ss.deleteStudent(student)) {
                    alert3.setHeaderText(null);
                    alert3.setContentText("Student has been remove succesfully!");
                    alert3.showAndWait();
                } else {
                    alert3.setHeaderText(null);
                    alert3.setContentText("An error has ocurred, please try later");
                    alert3.showAndWait();
                }
            }

        } else {
            if (ss.deleteStudent(student)) {
                Alert alert3 = new Alert(Alert.AlertType.INFORMATION);
                alert3.setHeaderText(null);
                alert3.setContentText("Student has been remove succesfully!");
                alert3.showAndWait();
            } else {
                Alert alert3 = new Alert(Alert.AlertType.ERROR);
                alert3.setHeaderText(null);
                alert3.setContentText("An error has ocurred, please try later");
                alert3.showAndWait();
            }
        }
    }

    public void loadStudents() {
        fxListDelete.getItems().clear();
        StudentServices cs = new StudentServices();
        List<Student> students;

        students = cs.getAllStudents();

        if (students != null) {
            fxListDelete.getItems().addAll(students);
        } else {
            Alert alert3 = new Alert(Alert.AlertType.ERROR);
            alert3.setHeaderText(null);
            alert3.setContentText("Students file couldn`t be loaded");
            alert3.showAndWait();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

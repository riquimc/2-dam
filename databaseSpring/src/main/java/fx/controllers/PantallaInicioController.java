/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import model.User;
import services.StudentServices;
import services.TeacherServices;

/**
 * FXML Controller class
 *
 * @author dam2
 */
public class PantallaInicioController implements Initializable {

    private User u;

    @FXML
    private BorderPane fxRoot;

    @FXML
    private MenuBar fxMenu;

    @FXML
    private Menu fxStudents;

    @FXML
    private Menu fxTeachers;

    @FXML
    private Menu fxSubjects;

    @FXML
    private MenuItem idGet;
    @FXML
    private MenuItem idAdd;
    @FXML
    private MenuItem fxDelete;

    @FXML
    private MenuItem fxUpdateStu;
    @FXML
    private MenuItem fxMarksStu;

    @FXML
    private MenuItem idUpdate1;
    @FXML
    private MenuItem markStuTeach;
    @FXML
    private MenuItem idGet1;
    @FXML
    private MenuItem idAdd1;
    @FXML
    private MenuItem idDeleteTeacher;

    @FXML
    private MenuItem idDelete;

    @FXML
    private AnchorPane pantallaLogin;
    @FXML
    private LoginController controllerLogin;
    @FXML
    private AnchorPane pantallaAdd;
    @FXML
    private PantallaAddStudentController controllerAdd;
    @FXML
    private AnchorPane pantallaGet;
    @FXML
    private PantallaGetController controllerGet;
    @FXML
    private AnchorPane addTeacherScene;
    @FXML
    private AddTeacherSceneController controllerAddTeacher;
    @FXML
    private AnchorPane getTeacherScene;
    @FXML
    private GetTeacherSceneController controllerGetTeacher;
    @FXML
    private AnchorPane deleteStudent;
    @FXML
    private DeleteStudentController deleteStudentController;
    @FXML
    private AnchorPane deleteTeacher;
    @FXML
    private DeleteTeacherController deleteTeacherController;
    @FXML
    private AnchorPane addSubject;
    @FXML
    private AddSubjectController addSubjectController;
    @FXML
    private AnchorPane getSubject;
    @FXML
    private GetSubjectsController getSubjectController;
    @FXML
    private AnchorPane updateStudent;
    @FXML
    private UpdateStudentController updateStudentController;
    @FXML
    private AnchorPane updateTeacher;
    @FXML
    private UpdateTeacherController updateTeacherController;
    @FXML
    private AnchorPane deleteSubject;
    @FXML
    private DeleteSubjectController deleteSubjectController;
    @FXML
    private AnchorPane enrol;
    @FXML
    private EnrolController enrolController;
    @FXML
    private AnchorPane getMarks;
    @FXML
    private GetMarksController getMarksController;
    @FXML
    private AnchorPane mark;
    @FXML
    private MarkController markController;
    @FXML
    private AnchorPane updateSubject;
    @FXML
    private UpdateSubjectController UpdateSubjectController;
    @FXML
    private AnchorPane logIn;

    public AnchorPane getPantallaLogin() {
        return pantallaLogin;
    }

    public LoginController getControllerLogin() {
        return controllerLogin;
    }

    public AnchorPane getPantallaAdd() {
        return pantallaAdd;
    }

    public PantallaAddStudentController getControllerAdd() {
        return controllerAdd;
    }

    public AnchorPane getPantallaGet() {
        return pantallaGet;
    }

    public PantallaGetController getControllerGet() {
        return controllerGet;
    }

    public AnchorPane getAddTeacherScene() {
        return addTeacherScene;
    }

    public AddTeacherSceneController getControllerAddTeacher() {
        return controllerAddTeacher;
    }

    public AnchorPane getGetTeacherScene() {
        return getTeacherScene;
    }

    public GetTeacherSceneController getControllerGetTeacher() {
        return controllerGetTeacher;
    }

    @FXML
    public void cargarPantallaLogin() {
        fxRoot.setCenter(pantallaLogin);
    }

    private void preCargaLogin() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/login.fxml"));
            pantallaLogin = loaderMenu.load();
            controllerLogin
                    = loaderMenu.getController();

            controllerLogin.setInicio(this);
        } catch (IOException ex) {

            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void preCargaGet() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/pantallaGetStudent.fxml"));
            pantallaGet = loaderMenu.load();
            controllerGet
                    = loaderMenu.getController();

            controllerGet.setInicio(this);

        } catch (IOException ex) {

            Logger.getLogger(PantallaGetController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void preCargaAdd() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/pantallaAddStudent.fxml"));
            pantallaAdd = loaderMenu.load();
            controllerAdd
                    = loaderMenu.getController();

            controllerAdd.setInicio(this);

        } catch (IOException ex) {

            Logger.getLogger(PantallaAddStudentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void preCargaGetTeacher() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/getTeacherScene.fxml"));
            getTeacherScene = loaderMenu.load();
            controllerGetTeacher
                    = loaderMenu.getController();

            controllerGetTeacher.setInicio(this);

        } catch (IOException ex) {

            Logger.getLogger(GetTeacherSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void preCargaAddTeacher() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/addTeacherScene.fxml"));
            addTeacherScene = loaderMenu.load();
            controllerAddTeacher
                    = loaderMenu.getController();

            controllerAddTeacher.setInicio(this);

        } catch (IOException ex) {

            Logger.getLogger(AddTeacherSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadDeleteStudent() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/deleteStudent.fxml"));
            deleteStudent = loaderMenu.load();
            deleteStudentController
                    = loaderMenu.getController();

            deleteStudentController.setInicio(this);

        } catch (IOException ex) {

            Logger.getLogger(AddTeacherSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadDeleteTeacher() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/deleteTeacher.fxml"));
            deleteTeacher = loaderMenu.load();
            deleteTeacherController
                    = loaderMenu.getController();

            deleteTeacherController.setInicio(this);

        } catch (IOException ex) {

            Logger.getLogger(AddTeacherSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadAddSubject() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/addSubject.fxml"));
            addSubject = loaderMenu.load();
            addSubjectController
                    = loaderMenu.getController();

            addSubjectController.setInicio(this);

        } catch (IOException ex) {

            Logger.getLogger(UpdateSubjectController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadGetSubject() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/getSubjects.fxml"));
            getSubject = loaderMenu.load();
            getSubjectController
                    = loaderMenu.getController();

            getSubjectController.setInicio(this);

        } catch (IOException ex) {

            Logger.getLogger(GetSubjectsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadUpdateStudent() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/updateStudent.fxml"));
            updateStudent = loaderMenu.load();
            updateStudentController
                    = loaderMenu.getController();

            updateStudentController.setInicio(this);

        } catch (IOException ex) {

            Logger.getLogger(AddTeacherSceneController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadUpdateTeacher() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/updateTeacher.fxml"));
            updateTeacher = loaderMenu.load();
            updateTeacherController
                    = loaderMenu.getController();

            updateTeacherController.setInicio(this);

        } catch (IOException ex) {
            Logger.getLogger(UpdateTeacherController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadDeleteSubject() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/deleteSubject.fxml"));
            deleteSubject = loaderMenu.load();
            deleteSubjectController
                    = loaderMenu.getController();

            deleteSubjectController.setInicio(this);

        } catch (IOException ex) {
            Logger.getLogger(UpdateTeacherController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadEnrol() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/enrol.fxml"));
            enrol = loaderMenu.load();
            enrolController
                    = loaderMenu.getController();

            enrolController.setInicio(this);

        } catch (IOException ex) {
            Logger.getLogger(UpdateTeacherController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadGetMarks() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/getMarks.fxml"));
            getMarks = loaderMenu.load();
            getMarksController
                    = loaderMenu.getController();

            getMarksController.setInicio(this);

        } catch (IOException ex) {
            Logger.getLogger(UpdateTeacherController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadMark() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/mark.fxml"));
            mark = loaderMenu.load();
            markController
                    = loaderMenu.getController();

            markController.setInicio(this);

        } catch (IOException ex) {
            Logger.getLogger(UpdateTeacherController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadUpdateSubject() {
        try {
            FXMLLoader loaderMenu = new FXMLLoader(
                    getClass().getResource(
                            "/fxml/updateSubject.fxml"));
            updateSubject = loaderMenu.load();
            UpdateSubjectController
                    = loaderMenu.getController();

            UpdateSubjectController.setInicio(this);

        } catch (IOException ex) {
            Logger.getLogger(UpdateTeacherController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void checkType(User user) {
        u = user;
        switch (user.getIdTypeUser()) {
            case 1:
                visibilityTrue();
                break;
            case 2:
                StudentServices ss = new StudentServices();
                fxStudents.setVisible(true);
                fxUpdateStu.setVisible(true);
                fxMarksStu.setVisible(true);
                idGet.setVisible(false);
                idAdd.setVisible(false);
                fxDelete.setVisible(false);

                updateStudentController.getFxStudents().setValue(ss.getStudent(user));
                updateStudentController.getFxStudents().setEditable(false);
                getMarksController.getFxStudents().disableProperty();

                getMarksController.getFxStudents().setValue(ss.getStudent(user));
                getMarksController.getFxStudents().disableProperty();

                break;
            case 3:
                TeacherServices ts = new TeacherServices();
                fxTeachers.setVisible(true);
                idUpdate1.setVisible(true);
                markStuTeach.setVisible(true);
                updateTeacherController.getFxTeachers().setValue(ts.getTeacher(u));
                updateTeacherController.getFxTeachers().setEditable(false);
                idAdd1.setVisible(false);
                idGet1.setVisible(false);
                idDeleteTeacher.setVisible(false);
                markController.loadCombos(user);
                break;

        }
    }

    private void visibilityTrue() {
        fxStudents.setVisible(true);
        fxSubjects.setVisible(true);
        fxTeachers.setVisible(true);

    }

    @FXML
    public void loadSceneGetStudents() {
        fxMenu.setVisible(true);
        fxRoot.setCenter(pantallaGet);
        controllerGet.loadStudents();
    }

    @FXML
    public void loadSceneAddStudents() {
        fxRoot.setCenter(pantallaAdd);
    }

    @FXML
    public void loadSceneGetTeachers() {
        fxRoot.setCenter(getTeacherScene);
        controllerGetTeacher.loadTeachers();
    }

    @FXML
    public void loadSceneUpdateTeachers() {
        updateTeacherController.loadTeachers();
        fxRoot.setCenter(updateTeacher);
    }

    @FXML
    public void loadSceneDeleteStudent() {
        fxRoot.setCenter(deleteStudent);
        deleteStudentController.loadStudents();
    }

    @FXML
    public void logOu() {
        try {
            if (null == logIn) {
                FXMLLoader loaderMenu = new FXMLLoader(
                        getClass().getResource(
                                "/fxml/login.fxml"));
                logIn = loaderMenu.load();
            }
            fxRoot.setCenter(logIn);
        } catch (IOException ex) {
            Logger.getLogger(PantallaInicioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void loadSceneDeleteTeachers() {
        fxRoot.setCenter(deleteTeacher);
        deleteTeacherController.loadTeachers();
    }

    @FXML
    public void loadSceneAddTeachers() {
        fxRoot.setCenter(addTeacherScene);

    }

    @FXML
    public void loadSceneAddSubjects() {
        addSubjectController.loadCombos();
        fxRoot.setCenter(addSubject);
    }

    @FXML
    public void loadSceneGetSubjects() {
        getSubjectController.loadSubjects();
        fxRoot.setCenter(getSubject);
    }

    @FXML
    public void loadSceneUpdateStudent() {
        updateStudentController.loadStudents();
        fxRoot.setCenter(updateStudent);
    }

    @FXML
    private void loadSceneDeleteSubject() {
        deleteSubjectController.loadSubjects();
        fxRoot.setCenter(deleteSubject);
    }

    @FXML
    private void loadSceneEnrol() {
        enrolController.loadListCombo();
        fxRoot.setCenter(enrol);
    }

    @FXML
    private void loadSceneGetMarksStudent() {
        getMarksController.loadCombos(u);
        fxRoot.setCenter(getMarks);
    }

    @FXML
    private void loadSceneMark() {
        markController.loadCombos(u);
        fxRoot.setCenter(mark);
    }

    @FXML
    private void loadSceneUpdateSubject() {
        UpdateSubjectController.loadTeachers();
        fxRoot.setCenter(updateSubject);
    }

    private void setVisibleFalse() {
        fxStudents.setVisible(false);
        fxSubjects.setVisible(false);
        fxTeachers.setVisible(false);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        preCargaLogin();
        preCargaGet();
        preCargaAdd();
        preCargaAddTeacher();
        preCargaGetTeacher();
        loadDeleteStudent();
        loadDeleteTeacher();
        loadGetSubject();
        loadAddSubject();
        loadUpdateStudent();
        loadUpdateTeacher();
        loadDeleteSubject();
        loadEnrol();
        loadGetMarks();
        loadMark();
        loadUpdateSubject();
        fxMenu.setVisible(false);
        setVisibleFalse();
        cargarPantallaLogin();
    }

}

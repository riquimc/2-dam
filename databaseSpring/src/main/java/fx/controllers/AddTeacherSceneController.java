package fx.controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import services.TeacherServices;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.scene.control.DatePicker;

/**
 * FXML Controller class
 *
 * @author Lucia
 */
public class AddTeacherSceneController implements Initializable {

    private PantallaInicioController inicio;

    private Alert alert;

    public Alert getAlert() {
        return alert;
    }

    @FXML
    private TextField fxNif;
    @FXML
    private TextField fxName;
    @FXML
    private TextField fxPhone;
    @FXML
    private DatePicker fxDoB;
    @FXML
    private TextField fxAddress;
    @FXML
    private TextField fxUserName;
    @FXML
    private TextField fxPassword;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }

    public void cleanValues() {
        fxNif.clear();
        fxName.clear();
        fxPhone.clear();
        fxDoB.setValue(null);
        fxAddress.clear();
        fxPassword.clear();
        fxUserName.clear();
    }

    public boolean checkEmptyFields() {
        return fxNif.getText().isEmpty() || fxName.getText().isEmpty()
                || fxPhone.getText().isEmpty() || fxDoB.getValue() == null
                || fxAddress.getText().isEmpty() || fxUserName.getText().isEmpty()
                || fxPassword.getText().isEmpty();
    }

    @FXML
    public void addTeacher() {
        TeacherServices ts = new TeacherServices();

        if (checkEmptyFields()) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.getDialogPane().lookupButton(ButtonType.OK).setId("alertOK");
            alert.setContentText("Please fill all data");
            alert.showAndWait();

        } else {

            if (ts.addTeacher(fxNif.getText(), fxName.getText(), fxPhone.getText(), fxDoB.getValue(), fxAddress.getText(), fxUserName.getText(),
                    fxPassword.getText())) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.getDialogPane().lookupButton(ButtonType.OK).setId("alertOK");
                alert.setContentText("New teacher saved");

                alert.showAndWait();
            } else {
                alert.getDialogPane().lookupButton(ButtonType.OK).setId("alertOK");
                alert.setContentText("teacher already exists");
                alert.showAndWait();
            }
        }
        cleanValues();
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import model.Student;
import services.StudentServices;

/**
 * FXML Controller class
 *
 * @author CAR
 */
public class UpdateStudentController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Alert alert;
    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }
    @FXML
    private ComboBox fxStudents;

    @FXML
    private TextField fxNif;
    @FXML
    private TextField fxName;
    @FXML
    private TextField fxPhone;
    @FXML
    private DatePicker fxDoB;
    @FXML
    private TextField fxAddress;
    @FXML
    private TextField fxUserName;
    @FXML
    private TextField fxPassword;

    private Student oldStudent;

    public ComboBox getFxStudents() {
        return fxStudents;
    }

    public void cleanValues() {
        fxNif.clear();
        fxName.clear();
        fxPhone.clear();
        fxDoB.setValue(null);
        fxAddress.clear();
        fxPassword.clear();
        fxUserName.clear();
    }

    public boolean checkEmptyFields() {
        return fxNif.getText().isEmpty() || fxName.getText().isEmpty()
                || fxPhone.getText().isEmpty() || fxDoB.getValue() == null
                || fxAddress.getText().isEmpty() || fxUserName.getText().isEmpty()
                || fxPassword.getText().isEmpty();
    }

    public void loadStudents() {
        fxStudents.getItems().clear();
        StudentServices ss = new StudentServices();
        fxStudents.getItems().addAll(ss.getAllStudents());

    }

    @FXML
    private void updateStudent() {
        StudentServices ss = new StudentServices();

        if (checkEmptyFields()) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please fill all data");
            alert.showAndWait();
        } else {
            if (ss.updateStudent(fxNif.getText(), fxName.getText(), fxPhone.getText(), fxDoB.getValue(), fxAddress.getText(), fxUserName.getText(), LocalDate.now().getYear() + "",
                    fxPassword.getText(),oldStudent)) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Student update");
                alert.showAndWait();
            } else {
                alert.setContentText("An error has occured please try later");
                alert.showAndWait();
            }
        }
        cleanValues();
    }

    @FXML
    private void showStudent() {
        Student student = (Student) fxStudents.getSelectionModel().getSelectedItem();
        fxNif.setText(student.getNif());
        fxName.setText(student.getFullName());
        fxPhone.setText(student.getPhone());
        fxDoB.setValue(student.getDateOfBirth());
        fxAddress.setText(student.getAddress());
        fxUserName.setText(student.getIduser());
        fxPassword.setText(student.getPassword());
        oldStudent = student;

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}

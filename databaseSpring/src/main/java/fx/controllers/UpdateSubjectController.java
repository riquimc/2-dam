/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import model.Subject;
import model.Teacher;
import services.SubjectServices;
import services.TeacherServices;

/**
 * FXML Controller class
 *
 * @author CAR
 */
public class UpdateSubjectController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Alert alert;
    private PantallaInicioController inicio;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }

    @FXML
    private TextField fxNameSubject;
    @FXML
    private ComboBox fxChooseTeacher;
    @FXML
    private ComboBox fxComboSubjects;

    private boolean checkEmpty(Teacher teacher) {
        return fxNameSubject.getText().isEmpty()
                || null == teacher;
    }

    @FXML
    private void fxAddSubject() {
        Teacher teacher = (Teacher) fxChooseTeacher.getSelectionModel().getSelectedItem();
        Subject subject = (Subject) fxComboSubjects.getSelectionModel().getSelectedItem();
        SubjectServices ss = new SubjectServices();

        if (checkEmpty(teacher)) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please fill all data");
            alert.showAndWait();
        } else {
            if (ss.updateSubject(fxNameSubject.getText(), subject.getIdsubject(), teacher.getIdMember())) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("New subject saved");
                alert.showAndWait();
            } else {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("subject already exists");
                alert.showAndWait();
            }
        }

    }

    @FXML
    private void fxShow() {
        Subject subject = (Subject) fxComboSubjects.getSelectionModel().getSelectedItem();

        fxNameSubject.setText(subject.getName());

    }

    public void loadTeachers() {
        fxChooseTeacher.getItems().clear();
        fxComboSubjects.getItems().clear();
        TeacherServices tt = new TeacherServices();
        SubjectServices ss = new SubjectServices();
        fxChooseTeacher.getItems().addAll(tt.getAllTeachers());
        fxComboSubjects.getItems().addAll(ss.getAllSubjects());

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

}

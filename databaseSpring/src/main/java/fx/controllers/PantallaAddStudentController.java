/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fx.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import services.StudentServices;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author dam2
 */
public class PantallaAddStudentController implements Initializable {

    private PantallaInicioController inicio;

    private Alert alert;

    public Alert getAlert() {
        return alert;
    }

    @FXML
    private TextField fxNif;
    @FXML
    private TextField fxName;
    @FXML
    private TextField fxPhone;
    @FXML
    private DatePicker fxDoB;
    @FXML
    private TextField fxAddress;
    @FXML
    private TextField fxUserName;
    @FXML
    private TextField fxPassword;

    public void setInicio(PantallaInicioController inicio) {
        this.inicio = inicio;
    }

    public void cleanValues() {
        fxNif.clear();
        fxName.clear();
        fxPhone.clear();
        fxDoB.setValue(null);
        fxAddress.clear();
        fxPassword.clear();
        fxUserName.clear();

    }

    public boolean checkEmptyFields() {
        return fxNif.getText().isEmpty() || fxName.getText().isEmpty()
                || fxPhone.getText().isEmpty() || fxDoB.getValue() == null
                || fxAddress.getText().isEmpty() || fxUserName.getText().isEmpty()
                || fxPassword.getText().isEmpty();
    }

    @FXML
    public void addStudent() {

        StudentServices ss = new StudentServices();

        if (checkEmptyFields()) {
            alert = new Alert(Alert.AlertType.ERROR);
            alert.getDialogPane().lookupButton(ButtonType.OK).setId("alertOK");
            alert.setContentText("Please fill all data");
            alert.showAndWait();

        } else {

            if (ss.addStudent(fxNif.getText(), fxName.getText(), fxPhone.getText(), fxDoB.getValue(), fxAddress.getText(), fxUserName.getText(), LocalDate.now().getYear() + "",
                    fxPassword.getText())) {
                alert = new Alert(Alert.AlertType.INFORMATION);
                alert.getDialogPane().lookupButton(ButtonType.OK).setId("alertOK");
                alert.setContentText("New student saved");

                alert.showAndWait();
            } else {
                alert.getDialogPane().lookupButton(ButtonType.OK).setId("alertOK");
                alert.setContentText("Student already exists");
                alert.showAndWait();
            }
        }
        cleanValues();
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        alert = new Alert(Alert.AlertType.ERROR);
        alert.getDialogPane().lookupButton(ButtonType.OK).setId("alertOK");
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.StudentsDAO;
import dao.StudentsJDBCDAO;
import model.Student;

import java.time.LocalDate;
import java.util.List;
import model.User;

public class StudentServices {

    public List<Student> getAllStudents() {
        StudentsDAO dao = new StudentsDAO();
        return dao.getAll();
    }

    public boolean addStudent(String nif, String name, String phone, LocalDate dateOfBirth, String address,
            String userName, String schoolYear, String password) {
        boolean ok = true;
        List<Student> students = this.getAllStudents();
        if (!students.stream().anyMatch((student) -> {
            return student.getNif().equals(nif);
        })) {
            StudentsDAO dao = new StudentsDAO();
            dao.save(new Student(schoolYear, userName, password, 2, 0, nif, name, address, phone, dateOfBirth));
        } else {
            ok = false;
        }
        return ok;
    }

    public boolean deleteStudent(Student student) {
        boolean ok = true;
        StudentsDAO dao = new StudentsDAO();
        if (!dao.delete(student)) {
            ok = false;
        }
        return ok;
    }

    public boolean updateStudent(String nif, String name, String phone, LocalDate dateOfBirth, String address,
            String userName, String schoolYear, String password, Student oldStudent) {
        boolean ok = true;
        StudentsDAO dao = new StudentsDAO();

        if (!dao.update(new Student(schoolYear, userName, password, 2, 0, nif,
                name, address, phone, dateOfBirth), oldStudent)) {
            ok = false;
        }

        return ok;
    }

    public List<Integer> getMarks(int idStudent, int idSubject) {
        StudentsDAO dao = new StudentsDAO();
        return dao.getMarks(idStudent, idSubject);

    }

    public Student getStudent(User u) {
        StudentsDAO dao = new StudentsDAO();
        return dao.get(u);
        
    }

    public boolean checkStudentHasSubjects(Student student) {
        StudentsJDBCDAO dao = new StudentsJDBCDAO();

        //dao.checkStudentHasSubjects(student);
        return false;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.SubjectJDBCDAO;
import dao.SubjectsDAO;
import java.util.List;
import model.Student;
import model.Subject;

/**
 *
 * @author CAR
 */
public class SubjectServices {

    public List<Subject> getAllSubjects() {
        SubjectsDAO dao = new SubjectsDAO();
        return dao.getAll();
    }

    public boolean addSubject(String name, int idsubject, int idteacher) {
        boolean ok = true;
        List<Subject> subjects = this.getAllSubjects();
        if (!subjects.stream().anyMatch((subject) -> {
            return subject.getName().equals(name);
        })) {
            SubjectsDAO dao = new SubjectsDAO();
            dao.save(new Subject(idsubject, idteacher, name));
        } else {
            ok = false;
        }
        return ok;
    }

    public boolean deleteSubject(Subject subject) {
        boolean ok = true;
        SubjectsDAO dao = new SubjectsDAO();
        if (!dao.delete(subject)) {
            ok = false;
        }

        return ok;
    }

    public boolean enrol(int idSubject, int idStudent) {
        boolean ok = true;
        SubjectsDAO dao = new SubjectsDAO();
        if (!dao.enroll(idStudent, idSubject)) {
            ok = false;
        }
        return ok;
    }

    public List<Student> getStudentsEnrolled(int idSubject) {
        SubjectsDAO dao = new SubjectsDAO();
        return dao.getStudentsEnrolled(idSubject);
    }

    public boolean updateSubject(String name, int idSubject, int teacherId) {
        boolean ok = true;
        SubjectsDAO dao = new SubjectsDAO();
        if (!dao.update(new Subject(idSubject, teacherId, name))) {
            ok = false;
        }

        return ok;

    }

    public boolean deleteStudent(Student st, Subject sb) {
        boolean ok = true;
        SubjectsDAO dao = new SubjectsDAO();
        if (!dao.deleteStudent(st, sb)) {
            ok = false;
        }

        return ok;

    }

    public List<Subject> getSubjectsFromStudents(Student st) {
        SubjectsDAO dao = new SubjectsDAO();
        return dao.getSubjectsFromStudent(st);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.UserDAO;
import model.User;

public class UserServices {

    public User checkUser(String user, String pass) {
        UserDAO dao = new UserDAO();
        return dao.getAll().stream().filter((us) -> {
            return (us.getIduser().equals(user)) && (us.getPassword().equals(pass));
        }).findFirst().get();
    }

}

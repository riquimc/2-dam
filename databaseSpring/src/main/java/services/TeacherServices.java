/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.TeacherJDBCDAO;
import dao.TeachersDAO;
import java.time.LocalDate;
import model.Teacher;

import java.util.List;
import model.Subject;
import model.User;

public class TeacherServices {

    public List<Teacher> getAllTeachers() {
        TeachersDAO dao = new TeachersDAO();
        return dao.getAll();
    }

    public boolean addTeacher(String nif, String name, String phone, LocalDate dateOfBirth, String address,
            String userName, String password) {
        boolean ok = true;
        List<Teacher> teachers = this.getAllTeachers();
        if (!teachers.stream().anyMatch((teacher) -> {
            return teacher.getNif().equals(nif);
        })) {
            TeachersDAO dao = new TeachersDAO();
            dao.save(new Teacher(LocalDate.now(), userName, password, 3, 0, nif, name, address, phone, dateOfBirth));
        } else {
            ok = false;
        }
        return ok;
    }

    public boolean deleteTeacher(Teacher teacher) {
        boolean ok = true;
        TeachersDAO dao = new TeachersDAO();
        if (!dao.delete(teacher)) {
            ok = false;
        }

        return ok;
    }

    public boolean updateTeacher(String nif, String name, String phone, LocalDate dateOfBirth, String address,
            String userName, LocalDate startDate, String password, Teacher oldTeacher) {
        boolean ok = true;
        TeachersDAO dao = new TeachersDAO();
        if (!dao.update(new Teacher(startDate, userName, password, 2, 0, nif,
                name, address, phone, dateOfBirth), oldTeacher)) {
            ok = false;
        }
        return ok;
    }

    public boolean markStudent(int mark, int numberCall, int idStudent, int idSubject) {
        TeachersDAO dao = new TeachersDAO();
        boolean ok = true;
        if (!dao.markStudent(mark, numberCall, idStudent, idSubject)) {
            ok = false;
        }
        return ok;
    }

    public List<Subject> loadSubjectsFromTeacher(User u) {
        TeachersDAO dao = new TeachersDAO();
        return dao.loadSubjectsFromTeacher(u);
    }

    public Teacher getTeacher(User u) {
        TeachersDAO dao = new TeachersDAO();
        return dao.get(u);

    }

}

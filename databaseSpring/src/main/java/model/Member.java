/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDate;

/**
 *
 * @author dam2
 */
public abstract class Member extends User{
    private int idMember;
    private String nif;
    private String fullName;
    private String address;
    private String phone;
    private LocalDate dateOfBirth;

    public Member(String iduser,String password,int idTypeUser,int idMember, String nif, String fullName, String address, String phone, LocalDate dateOfBirth) {
        super(iduser,password, idTypeUser);
        this.idMember = idMember;
        this.nif = nif;
        this.fullName = fullName;
        this.address = address;
        this.phone = phone;
        this.dateOfBirth = dateOfBirth;
    }
    public Member(){
        
    }

    public int getIdMember() {
        return idMember;
    }

    public void setIdMember(int idMember) {
        this.idMember = idMember;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.idMember;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Member other = (Member) obj;
        if (this.idMember != other.idMember) {
            return false;
        }
        return true;
    }
    
    
}

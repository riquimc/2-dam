/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDate;

/**
 * @author vic
 */
public class Student extends Member {

    private String schoolYear;

    public Student(String schoolYear, String iduser, String password, int idTypeUser, int idMember, String nif, 
            String fullName, String address, String phone, LocalDate dateOfBirth) {
        super(iduser, password, idTypeUser, idMember, nif, fullName, address, phone, dateOfBirth);
        this.schoolYear = schoolYear;
    }
    public Student(){
        
    }

    public void setSchoolYear(String schoolYear) {
        this.schoolYear = schoolYear;
    }

    public String getSchoolYear() {
        return schoolYear;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + super.getFullName() + ", NIF= " + super.getNif() + '}';
    }

}

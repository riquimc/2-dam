/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.time.LocalDate;

public class Teacher extends Member {

    private LocalDate startDate;

    
    public Teacher(LocalDate startDate, String iduser, String password, int idTypeUser, int idMember, String nif, String fullName, String address, String phone, LocalDate dateOfBirth) {
        super(iduser, password, idTypeUser, idMember, nif, fullName, address, phone, dateOfBirth);
        this.startDate = startDate;
    }

    public Teacher(){
        
    }
    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    @Override
    public String toString() {
        return "Teacher{" + "name=" + super.getFullName() + ", NIF= " + super.getNif()+ '}';
    }

}

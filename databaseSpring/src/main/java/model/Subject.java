/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author CAR
 */
public class Subject {

    private int idsubject;
    private int idTeacher;
    private String name;

    public Subject(int idsubject, int idTeacher, String name) {
        this.idsubject = idsubject;
        this.idTeacher = idTeacher;
        this.name = name;
    }

    public Subject() {

    }

    public int getIdsubject() {
        return idsubject;
    }

    public void setIdsubject(int idsubject) {
        this.idsubject = idsubject;
    }

    public int getIdTeacher() {
        return idTeacher;
    }

    public void setIdTeacher(int idTeacher) {
        this.idTeacher = idTeacher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Subject{" + "idSubject=" + idsubject + ", idTeacher=" + idTeacher + ", name=" + name + '}';
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author CAR
 */
public class User {

    private String iduser;
    private String password;
    private int idTypeUser;

    public User(String iduser, String password, int idTypeUser) {
        this.iduser = iduser;
        this.password = password;
        this.idTypeUser = idTypeUser;
    }

    public User() {
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getIdTypeUser() {
        return idTypeUser;
    }

    public void setIdTypeUser(int idTypeUser) {
        this.idTypeUser = idTypeUser;
    }

}

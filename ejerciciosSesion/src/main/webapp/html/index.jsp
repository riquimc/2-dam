<%-- 
    Document   : newjsp
    Created on : 28 sept. 2019, 21:08:02
    Author     : CAR
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:out value="${visita}"/><br>
        <c:out value="${ultimaVisita}"/><br><br><br>
        <form method="GET">
            <input type="submit" value="Iniciar sesion"/>
        </form>
        <form method="POST">
            <input type="submit" value="Cerrar sesion"/>
        </form>
    </body>
</html>

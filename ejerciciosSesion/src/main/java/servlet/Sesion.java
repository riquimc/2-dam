/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author CAR
 */
@WebServlet(name = "Sesion", urlPatterns = {"/sesion"})
public class Sesion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int visitas = 0;
        LocalTime ultimaConexion;
        LocalTime conexionActual = LocalTime.now();
        String stringResponse = "";

        if (null != request.getSession().getAttribute("contador")
                && null != request.getSession().getAttribute("ultima conexion")) {
            visitas = (Integer) request.getSession().getAttribute("contador");
            ultimaConexion = (LocalTime) request.getSession().getAttribute("ultima conexion");

            //request.setAttribute("ultimaVisita", "ultima conexión hace:" + Duration.between(ultimaConexion, conexionActual).getSeconds() + " segundos");
            stringResponse = "ultima conexión hace: " + Duration.between(ultimaConexion, conexionActual).getSeconds() + " segundos";
        }

        visitas++;

        request.getSession().setAttribute("contador", visitas);
        request.getSession().setAttribute("ultima conexion", conexionActual);

        // request.setAttribute("visita", "visitas:" + visitas);
        //  request.getRequestDispatcher("html/index.jsp").forward(request, response);
        stringResponse = "visitas:" + visitas + "\n" + stringResponse;
        response.getWriter().print(stringResponse);
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package privado;

import config.ConfigYAML;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import filtro.LogInFilter;
import utils.Constantes;

/**
 *
 * @author CAR
 */
@WebServlet(name = "ElegirInformes", urlPatterns = {Constantes.ELEGIR_INFORMES_URL})
public class ElegirInformes extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            if (Integer.parseInt(request.getSession().getAttribute(Constantes.NUMERO_DEL_INFORME).toString())
                    <= ConfigYAML.getInstance().getInformes().size()) {
                request.getSession().setAttribute(Constantes.INFORME_A_MOSTRAR,
                        ConfigYAML.getInstance().getInformes().get(Integer.parseInt(request.getSession().
                                getAttribute(Constantes.NUMERO_DEL_INFORME).toString())));
                request.getSession().setAttribute(Constantes.ACCEDER_A_INFORMES, Constantes.ACCEDER_A_INFORMES_VALUE_SI);
            } else {
                request.getSession().setAttribute(Constantes.ACCEDER_A_INFORMES, Constantes.ACCEDER_A_INFORMES_VALUE_NO);
            }
        }catch (Exception e){
            Logger.getLogger(LogInFilter.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cf.CifradoCesar;
import utils.Constantes;

/**
 * @author dam2
 */
@WebServlet(name = "LogIn", urlPatterns = {Constantes.LOG_IN_URL})
public class LogIn extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (null == request.getSession().getAttribute(Constantes.USER)
                && null == request.getSession().getAttribute(Constantes.PASSWORD)) {
            CifradoCesar cf = new CifradoCesar();
            request.getSession().setAttribute(Constantes.USER, cf.descifra(request.getParameter(Constantes.USER),
                    Integer.parseInt(request.getParameter(Constantes.CLAVE))));

            request.getSession().setAttribute(Constantes.PASSWORD, cf.descifra(request.getParameter(Constantes.PASSWORD),
                    Integer.parseInt(request.getParameter(Constantes.CLAVE))));
            request.getSession().setAttribute(Constantes.CLAVE, request.getParameter(Constantes.CLAVE));
            request.getSession().setAttribute(Constantes.LOG_IN_ATTRIBUTE, Constantes.LOG_IN_ATTRIBUTE_VALUE);

            response.getWriter().print("YES");
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}

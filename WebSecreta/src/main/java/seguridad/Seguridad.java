/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguridad;

import cf.CifradoCesar;
import com.google.common.base.Strings;

import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

import utils.Constantes;

/**
 * @author CAR
 */
public class Seguridad {

    public boolean LogInCorrecto(HttpServletRequest request) {
        String clave = request.getParameter(Constantes.CLAVE).chars().filter((arg0) -> {
            return arg0 <= 57 && arg0 >= 48;
        }).mapToObj(value -> String.valueOf((char) value)).collect(Collectors.joining());

        boolean acces = false;
        CifradoCesar cf = new CifradoCesar();
        if (((!Strings.isNullOrEmpty(request.getParameter(Constantes.USER))
                && !Strings.isNullOrEmpty(request.getParameter(Constantes.PASSWORD))
                && !Strings.isNullOrEmpty(request.getParameter(Constantes.CLAVE)))

                && (config.ConfigYAML.getInstance().getUsuarios().stream().anyMatch((user) -> {
            return user.getNombre().equals(cf.descifra(request.getParameter(Constantes.USER),
                    Integer.parseInt(request.getParameter(Constantes.CLAVE))))
                    && user.getPassword().equals(cf.descifra(request.getParameter(Constantes.PASSWORD),
                    Integer.parseInt(request.getParameter(Constantes.CLAVE))));
        }))) 
                && ((clave.length() == request.getParameter(Constantes.CLAVE).length()))) {
            acces = true;
        }

        return acces;
    }

}

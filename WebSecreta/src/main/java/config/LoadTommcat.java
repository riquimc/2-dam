package config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import utils.Constantes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Web application lifecycle listener.
 *
 * @author CAR
 */
@WebListener()
public class LoadTommcat implements ServletContextListener {

    public LoadTommcat() {
    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {

        ConfigYAML.getInstance(arg0.getServletContext()
                .getResourceAsStream(Constantes.CONFIG_YAML_LOCATION));

    }

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
    }
}

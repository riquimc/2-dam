/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package config;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import modelo.Usuario;
import org.yaml.snakeyaml.Yaml;

/**
 *
 * @author CAR
 */
public class ConfigYAML {

    private static ConfigYAML config;

    private ConfigYAML() {
    }

    public static ConfigYAML getInstance() {
        return config;
    }

    public static ConfigYAML getInstance(InputStream file) {
        if (config == null) {
            Yaml yaml = new Yaml();
            config = (ConfigYAML) yaml.loadAs(
                    file, ConfigYAML.class);
        }
        return config;
    }

  
    private Map<Integer, String> informes;
    private List<String> pisos;
    private List<Usuario> usuarios;

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    

    public Map<Integer, String> getInformes() {
        return informes;
    }

    public void setInformes(Map<Integer, String> informes) {
        this.informes = informes;
    }


    public List<String> getPisos() {
        return pisos;
    }

    public void setPisos(List<String> pisos) {
        this.pisos = pisos;
    }


}

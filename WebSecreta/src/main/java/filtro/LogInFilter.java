/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filtro;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import seguridad.Seguridad;
import utils.Constantes;

/**
 * @author CAR
 */
@WebFilter(filterName = "LogInFilter", urlPatterns = {Constantes.LOG_IN_URL, Constantes.FILTRO_PRIVADO_URL})
public class LogInFilter implements Filter {

    private FilterConfig filterConfig = null;

    public LogInFilter() {
    }

    /**
     * @param request  The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain    The filter chain we are processing
     * @throws IOException      if an input/output error occurs
     * @throws ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
            throws IOException, ServletException {

        Seguridad seguridad = new Seguridad();
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        try {
            if (null == ((HttpServletRequest) request).getSession().getAttribute(Constantes.LOG_IN_ATTRIBUTE)) {
                if (seguridad.LogInCorrecto(httpRequest)) {
                    chain.doFilter(request, response);
                } else {
                    response.getWriter().print("ERROR");
                }
            } else {
                chain.doFilter(request, response);
            }
        } catch (IOException | ServletException e) {
            Logger.getLogger(LogInFilter.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("LoggIn()");
        }
        StringBuffer sb = new StringBuffer("LoggIn(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }

    @Override
    public void init(FilterConfig arg0) throws ServletException {
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filtro;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import cf.CifradoCesar;
import utils.Constantes;

/**
 * @author dam2
 */
@WebFilter(filterName = "datosCifrados", urlPatterns = {Constantes.FILTRO_DATOS_CIFRADOS_URLS})
public class datosCifrados implements Filter {

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured. 
    private FilterConfig filterConfig = null;

    public datosCifrados() {
    }

    private void doBeforeProcessing(HttpServletRequest httpRequest, int clave, CifradoCesar cf)
            throws IOException, ServletException {

        if (httpRequest.getRequestURI().equals(Constantes.ELEGIR_INFORMES_URI)) {
            httpRequest.getSession().setAttribute(Constantes.NUMERO_DEL_INFORME,
                    cf.descifra(httpRequest.getParameter(Constantes.NUM_INFORME_PARAMETER), clave));
        }
    }

    private void doAfterProcessing(HttpServletRequest httpRequest, ServletResponse response, int clave, CifradoCesar cf)
            throws IOException, ServletException {
        if (httpRequest.getRequestURI().equals(Constantes.INFORMACION_DEL_USUARIO_URI)) {

            response.getWriter().print(cf.cifra("nombre: " + httpRequest.getSession()
                    .getAttribute(Constantes.NOMBRE_USUARIO).toString() + "\n" +
                    "edad: " + httpRequest.getSession()
                    .getAttribute(Constantes.EDAD_USUARIO).toString() + "\n" +
                    "direccion: " + httpRequest.getSession()
                    .getAttribute(Constantes.DIRECCION_USUARIO).toString(), clave));
        }

        if (httpRequest.getRequestURI().equals(Constantes.ELEGIR_INFORMES_URI)
                && httpRequest.getSession().getAttribute(Constantes.ACCEDER_A_INFORMES)
                .equals(Constantes.ACCEDER_A_INFORMES_VALUE_SI)) {
            response.getWriter().print(cf.cifra((String) httpRequest.getSession().getAttribute(Constantes.INFORME_A_MOSTRAR), clave));
        } else {
            httpRequest.getSession().setAttribute(Constantes.ACCEDER_A_INFORMES, Constantes.ACCEDER_A_INFORMES_VALUE_SI);
        }

        if (httpRequest.getRequestURI().equals(Constantes.MOSTRAR_PISOS_URI)) {
            response.getWriter().print(cf.cifra((String) httpRequest.getSession().getAttribute(Constantes.LISTA_DE_PISOS_ATTRIBUTE),
                    clave));
        }
    }

    /**
     * @param request  The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain    The filter chain we are processing
     * @throws IOException      if an input/output error occurs
     * @throws ServletException if a servlet error occurs
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        CifradoCesar cf = new CifradoCesar();
        int clave = Integer.parseInt(httpRequest.getSession().getAttribute(Constantes.CLAVE).toString());

        doBeforeProcessing(httpRequest, clave, cf);

        chain.doFilter(request, response);

        doAfterProcessing(httpRequest, response, clave, cf);

    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    @Override
    public void destroy() {
    }

    /**
     * Init method for this filter
     *
     * @param filterConfig
     */
    @Override
    public void init(FilterConfig filterConfig) {
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("datosCifrados()");
        }
        StringBuffer sb = new StringBuffer("datosCifrados(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
    }
}

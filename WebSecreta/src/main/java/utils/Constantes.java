/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author CAR
 */
public class Constantes {

    public static final String USER = "user";
    public static final String PASSWORD = "pass";
    public static final String CLAVE = "clave";
    public static final String LOG_IN_ATTRIBUTE = "LogIn";
    public static final String LOG_IN_ATTRIBUTE_VALUE = "hecho";
    public static final String LISTA_DE_PISOS_ATTRIBUTE = "listaDePisos";
    public static final String NOMBRE_USUARIO = "nombre";
    public static final String EDAD_USUARIO = "edad";
    public static final String DIRECCION_USUARIO = "direccion";
    public static final String INFORME_A_MOSTRAR = "informeAMostrar";
    public static final String INFORME_MOSTRADO_ATTRIBUTE = "informeMostrado";
    public static final String NUMERO_DEL_INFORME = "numeroDelInforme";
    public static final String NUM_INFORME_PARAMETER = "numInforme";
    public static final String NOMBRE_USUARIO_ATTRIBUTE = "nombreUsuario";
    public static final String EDAD_USUARIO_ATTRIBUTE = "edadUsuario";
    public static final String DIRECCION_USUARIO_ATTRIBUTE = "direccionUsuario";
    public static final String PISOS_A_MOSTRAR = "pisosAMostrar";
    public static final String ACCEDER_A_INFORMES = "accesoAInformes";
    public static final String ACCEDER_A_INFORMES_VALUE_SI = "si";
    public static final String ACCEDER_A_INFORMES_VALUE_NO = "no";

    public static final String IR_A_LOG_IN = "/index.html";
    public static final String CERRAR_SESION_URL = "/privado/cerrarSesion";
    public static final String IR_PAGPRINCIPAL = "/privado/PagPrincipal.html";
    public static final String LOG_IN_URL = "/logIn";
    public static final String MOSTRAR_PISOS_URL = "/privado/encriptado/mostrarPisos";
    public static final String MOSTRAR_PISOS_URI = "/WebSecreta_war/privado/encriptado/mostrarPisos";
    public static final String INFORMACION_DEL_USUARIO_URL = "/privado/encriptado/informacionUsuario";
    public static final String INFORMACION_DEL_USUARIO_URI = "/WebSecreta_war/privado/encriptado/informacionUsuario";
    public static final String ELEGIR_INFORMES_URL = "/privado/encriptado/elegirInformes";
    public static final String ELEGIR_INFORMES_URI = "/WebSecreta_war/privado/encriptado/elegirInformes";
    public static final String FILTRO_DATOS_CIFRADOS_URLS = "/privado/encriptado/*";
    public static final String IR_A_INFORMACION_DEL_USUARIO = "/privado/InformacionUsuario.jsp";
    public static final String IR_A_MOSTRAR_INFORMES = "/privado/MostrarInforme.jsp";
    public static final String IR_A_MOSTRAR_PISOS = "/privado/MostrarPisos.jsp";
    public static final String FILTRO_PRIVADO_URL = "/privado/*";
    public static final String ERROR_USUARIO_O_CONTRASEÑA_INCORRECTOS = "/paginasError/UsuarioContraseñaIncorrecto.html";
    public static final String ERROR_CLAVE_NO_ES_UN_NUMERO = "/paginasError/ClaveNoEsUnNumero.html";
    public static final String ERROR_SESION_INICIADA = "/paginasError/SesionIniciada.html";
    public static final String ERROR_CAMPOS_VACIOS = "/paginasError/CamposLoggInVacios.html";
    public static final String ERROR_INFORME_NO_EXISTE = "/paginasError/InformeNoExiste.html";

    public static final String CONFIG_YAML_LOCATION = "/WEB-INF/config/config.yml";

}

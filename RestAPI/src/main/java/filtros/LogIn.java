package filtros;

import servicios.ServiciosUsuarios;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(filterName = "LogIn", urlPatterns = {"/logIn"})
public class LogIn implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        ServiciosUsuarios su = new ServiciosUsuarios();
        if (su.getUsers().stream().anyMatch(user ->
                user.getNombre().equals(req.getParameter("nombre")) && user.getPassword().equals(req.getParameter("password")))) {
            resp.getWriter().print("OK");
        } else {
            resp.getWriter().print("ERROR");
        }


        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}

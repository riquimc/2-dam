package modelo;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;

public class Usuario {

    private int iduser;
    private String nombre;
    private String password;

    @JsonbCreator
    public Usuario(@JsonbProperty("iduser") int iduser, @JsonbProperty("nombre") String nombre, @JsonbProperty("password") String password) {
        this.iduser = iduser;
        this.nombre = nombre;
        this.password = password;
    }

    public Usuario() {

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}

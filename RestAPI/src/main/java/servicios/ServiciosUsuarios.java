package servicios;

import dao.UserDAO;
import modelo.Usuario;

import java.util.List;

public class ServiciosUsuarios {

    public List<Usuario> getUsers() {
        UserDAO dao = new UserDAO();
        return dao.getUsers();
    }
}

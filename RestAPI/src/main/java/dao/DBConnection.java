package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {
    public String urlDB;
    public String userName;
    public String password;

    public Connection getConnection() {
        Connection conn = null;
        try {
            conn = DBConnPool.getInstance().getConnection();

        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return conn;
        }
    }

    public static void closeConnection(Connection connArg) {
        try {
            if (connArg != null) {
                connArg.close();
            }
        } catch (Exception sqle) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, sqle);
        }
    }

    public void releaseResource(PreparedStatement pstmt) {
        try {
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (SQLException sqle) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, sqle);
        }
    }

    public void releaseResource(ResultSet rs) {
        try {
            if (rs != null) {
                rs.close();
            }
        } catch (Exception sqle) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, sqle);
        }
    }


}

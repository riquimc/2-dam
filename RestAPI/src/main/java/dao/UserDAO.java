package dao;

import modelo.Usuario;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class UserDAO {

    public List<Usuario> getUsers() {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getDataSource());
        return jtm.query("select * from user", BeanPropertyRowMapper.newInstance(Usuario.class));
    }
}
